package at.ac.tuwien.ifs.sge.core.engine.game.tournament;

import at.ac.tuwien.ifs.sge.core.engine.factory.MatchFactory;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.util.pair.ImmutablePair;
import at.ac.tuwien.ifs.sge.core.util.pair.MutablePair;
import com.google.common.math.IntMath;

import java.security.cert.CertificateEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class RoundRobin extends AbstractTournament {

  protected Map<ImmutablePair<Agent, Agent>, Double> oneVsOneResult = new HashMap<>();

  protected boolean oneVsOne = false;

  public RoundRobin(MatchFactory matchFactory,
                    List<Agent> gameAgents,
                    int numberOfPlayers,
                    Logger log) {
    super(matchFactory,
            gameAgents,
            numberOfPlayers,
            log);
    this.oneVsOne = numberOfPlayers == 2;
  }



  @Override
  public String toTextRepresentation() {
    if (tournamentResults.size() == 0) {
      return "not played";
    } else if (textRepresentation == null) {
      if (oneVsOne) {
        textRepresentation = createOneVsOneStringTable();
      } else {
        textRepresentation = createResultTable();
      }
    }

    return textRepresentation;
  }

  @Override
  public List<MatchResult> call() throws ExecutionException, InterruptedException, CertificateEncodingException {
    if (tournamentResults.size() == 0) {
      int gameAgentsSize = gameAgents.size();
      int numberOfGames = IntMath.binomial(gameAgentsSize, nrOfPlayersPerMatch);

      log.info("Starting Round Robin tournament with " + gameAgentsSize + " contestants, "
              + numberOfGames + " game" + (numberOfGames == 1 ? "" : "s") + ".");

      log.debug(
              "Agents: " + gameAgents.stream().map(Objects::toString).collect(Collectors.joining(", ")));

      if (oneVsOne) {
        oneVsOneTournament();
      } else {
        tournament();
      }
    }

    return Collections.unmodifiableList(tournamentResults);
  }
  @Override
  public TournamentMode getMode() {
    return TournamentMode.ROUND_ROBIN;
  }

  public boolean isOneVsOne() {
    return oneVsOne;
  }

  public Map<ImmutablePair<Agent, Agent>, Double> getOneVsOneResult() {
    return oneVsOneResult;
  }

  private void oneVsOneTournament() throws ExecutionException, InterruptedException, CertificateEncodingException {
    for (int x = 0; x < gameAgents.size(); x++) {
      var xAgent = gameAgents.get(x);
      for (int y = x + 1; y < gameAgents.size(); y++) {
        var yAgent = gameAgents.get(y);
        var match = createOneVsOneMatch(xAgent, yAgent);
        nextMatchSubject.onNext(match);

        var matchResult = match.call();
        onMatchResult(matchResult);

      }
    }
  }

  private void tournament() throws ExecutionException, InterruptedException, CertificateEncodingException {
    var indices = new int[nrOfPlayersPerMatch];

    for (int i = 0; i < indices.length; i++) {
      indices[i] = i;
    }

    do {
      List<Agent> agentList = Arrays.stream(indices).mapToObj(gameAgents::get).toList();
      var playerMap = Util.getPlayerMap(agentList);
      var match = matchFactory.newMatch(playerMap);
      nextMatchSubject.onNext(match);

      MatchResult matchResult;

      matchResult = match.call();
      onMatchResult(matchResult);

      indices = Util.combinations(indices);
    } while (indices[nrOfPlayersPerMatch - 1] < gameAgents.size());
  }

  protected Match<?, ?> createOneVsOneMatch(Agent agent0, Agent agent1) {
    var playerMap = new HashMap<Integer, Agent>(2);
    playerMap.put(0, agent0);
    playerMap.put(1, agent1);
    return matchFactory.newMatch(playerMap);
  }

  protected void onMatchResult(MatchResult matchResult) {
    tournamentResults.add(matchResult);
    if (oneVsOne) {
      var utility = matchResult.getResult();
      var agents = matchResult.getGameAgents();
      var matchUp = new ImmutablePair<>(agents.get(0), agents.get(1));
      var win = utility[0] > 0 ? 1D : -1D;
      if (utility[0] <= 0 && utility[1] <= 0)
        win = 0;
      oneVsOneResult.put(matchUp, win);
    } else {
      var utility = matchResult.getResult();
      var score = Util.scoresOutOfUtilities(utility);
      var agents = matchResult.getGameAgentsByPlayerId();
      for (var entrySet : agents.entrySet()) {
        var id = entrySet.getKey();
        var agent = entrySet.getValue();
        if (result.containsKey(agent)) {
          var agentResult = result.get(agent);
          agentResult.setA(agentResult.getA() + utility[id]);
          agentResult.setB(agentResult.getB() + score[id]);
        } else {
          result.put(agent, new MutablePair<>(utility[id], score[id]));
        }
      }
    }
    nextMatchResultSubject.onNext(matchResult);
  }



  protected String createOneVsOneStringTable() {
    StringBuilder stringBuilder = new StringBuilder();

    List<String> gameAgentNames = gameAgents.stream()
            .map(Agent::toString).collect(Collectors.toList());
    List<Integer> gameAgentWidths = gameAgentNames.stream()
            .map(String::length).collect(Collectors.toList());
    int maxWidth = gameAgentWidths.stream().mapToInt(Integer::intValue).max().orElse(0);

    MatchResult.appendSeparationLine(stringBuilder, maxWidth, gameAgentWidths).append('|');

    Util.appendNTimes(stringBuilder, ' ', maxWidth + 2);
    stringBuilder.append('|').append(' ').append(String.join(" | ", gameAgentNames)).append(' ')
            .append('|').append('\n');

    MatchResult.appendSeparationLine(stringBuilder, maxWidth, gameAgentWidths);

    for (var y = 0; y < gameAgents.size(); y++) {
      var yAgent = gameAgents.get(y);
      String yAgentName = yAgent.toString();
      stringBuilder.append('|');
      Util.appendWithBlankBuffer(stringBuilder, yAgentName, maxWidth + 2).append('|');
      for (var x = 0; x < gameAgents.size(); x++) {
        var xAgent = gameAgents.get(x);
        var matchUp = new ImmutablePair<>(yAgent, xAgent);
        if (oneVsOneResult.containsKey(matchUp)) {
          stringBuilder.append(' ');
          Util.appendWithBlankBuffer(stringBuilder, oneVsOneResult.get(matchUp),
                  gameAgentWidths.get(x)).append(' ');
        } else {
          Util.appendNTimes(stringBuilder, ' ', gameAgentWidths.get(x) + 2);
        }
        stringBuilder.append('|');
      }
      stringBuilder.append('\n');
    }

    MatchResult.appendSeparationLine(stringBuilder, maxWidth, gameAgentWidths)
            .deleteCharAt(stringBuilder.length() - 1);

    return stringBuilder.toString();
  }

}
