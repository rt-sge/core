package at.ac.tuwien.ifs.sge.core.engine.factory;

import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class GameFactory<G extends Game<?, ?>> implements Factory<G> {

  private final Constructor<G> gameConstructor;
  private final Logger log;

  private final G testGame;

  public GameFactory(Constructor<G> gameConstructor,
                     G testGame,
                     Logger log) {
    this.gameConstructor = gameConstructor;
    this.testGame = testGame;
    this.log = log;
  }

  @Override
  public G newInstance(Object... initargs) {
    try {
      var newArgs = new Object[initargs.length + 1];
      System.arraycopy(initargs, 0, newArgs, 0, initargs.length);
      newArgs[initargs.length] = log;
      return gameConstructor.newInstance(newArgs);
    } catch (InstantiationException e) {
      log._error_();
      log.error("Could not instantiate new element with constructor of game");
      log.printStackTrace(e);
    } catch (IllegalAccessException e) {
      log._error_();
      log.error("Could not access constructor of game");
      log.printStackTrace(e);
    } catch (InvocationTargetException e) {
      log._error_();
      log.error("Could not invoke constructor of game");
      log.printStackTrace(e);
    }
    throw new IllegalStateException("GameFactory is faulty");

  }

  public int getMinimumNumberOfPlayers() {
    return testGame.getMinimumNumberOfPlayers();
  }

  public int getMaximumNumberOfPlayers() {
    return testGame.getMaximumNumberOfPlayers();
  }

  public boolean isRealtime() {
    return testGame.isRealtime();
  }

  public String getHumanAgentClassName() {
    return testGame.getHumanAgentClassName();
  }

  public String getConfigEditorClassName() {
    return testGame.getConfigurationEditorClassName();
  }

  public GameConfiguration getGameConfiguration() {
    return testGame.getGameConfiguration();
  }
}
