package at.ac.tuwien.ifs.sge.core.engine.communication.events;

import java.io.Serializable;

public class SgeEvent implements Serializable {

    protected int playerId;

    public SgeEvent() {}

    public SgeEvent(int playerId) {
        this.playerId = playerId;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }
}
