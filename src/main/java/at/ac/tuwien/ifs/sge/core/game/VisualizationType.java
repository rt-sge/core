package at.ac.tuwien.ifs.sge.core.game;

public enum VisualizationType {
    text,
    javafx
}
