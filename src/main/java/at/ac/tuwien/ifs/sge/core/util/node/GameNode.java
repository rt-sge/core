package at.ac.tuwien.ifs.sge.core.util.node;

import at.ac.tuwien.ifs.sge.core.game.TurnBasedGame;

public interface GameNode<A> {


  TurnBasedGame<A, ?> getGame();

  void setGame(TurnBasedGame<A, ?> game);

}
