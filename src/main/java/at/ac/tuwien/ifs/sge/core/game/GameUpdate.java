package at.ac.tuwien.ifs.sge.core.game;

import java.util.List;

public class GameUpdate<A> {

    private final List<A> actions;
    private final int player;
    private final long executionTimeMs;

    public GameUpdate(List<A> actions, int player, long executionTimeMs) {
        this.actions = actions;
        this.player = player;
        this.executionTimeMs = executionTimeMs;
    }


    public List<A> getActions() {
        return actions;
    }

    public int getPlayer() {
        return player;
    }

    public long getExecutionTimeMs() {
        return executionTimeMs;
    }
}
