package at.ac.tuwien.ifs.sge.core.game;

import java.util.List;

/**
 * A turn based game.
 *
 * @param <A> - Type of Action
 * @param <B> - Type of Board
 */
public interface TurnBasedGame<A, B> extends Game<A, B> {


    //region General

    /**
     * Checks which player's move it is and returns the id of the player. A negative number indicates
     * some indeterminacy which is resolved by the game itself.
     *
     * @return the id of the player
     */
    int getCurrentPlayer();

    /**
     * The game as seen from the given player. In games with complete and perfect information or
     * non-canonical games (games where this function was already called) this method will return a
     * copy of this game as is. In other games the unknown information will be hidden and abstracted
     * via placeholders.
     *
     * @param player - the player
     * @return a copy of the game with only the information available to the player
     */
    TurnBasedGame<A, B> getGame(int player);

    /***
     * The game as seen from the current player. In games with complete and perfect information or
     * non-canonical games (games where this function was already called) this method will return a
     * copy of this game as is. In other games the unknown information will be hidden and abstracted
     * via placeholders.
     *
     * @return a copy of the game with only the information available to the player
     */
    default TurnBasedGame<A, B> getGame() {
        return getGame(this.getCurrentPlayer());
    }

    /**
     * Disqualifies the player from the game before it is over
     *
     * If this method is not implemented, games with more than 2 players will end
     * as soon as one player gets disqualified!
     *
     * @param playerId of the player to
     * @return true if the player was disqualified successfully
     */
    default boolean disqualifyPlayer(int playerId) { return false; }

    //endregion

    //region Utility

    /**
     * The weight of the utility function of a given player. Per default 0 if player or the
     * currentPlayer is less than 0 and 1 iff getCurrentPlayer() equals player otherwise -1.
     *
     * @param player - the player
     * @return the weight of the utility function.
     */
    default double getPlayerUtilityWeight(int player) {
        if (player < 0 || getCurrentPlayer() < 0) {
            return 0;
        }
        if (getCurrentPlayer() == player) {
            return 1;
        }
        return (-1);
    }

    //endregion

    //region Actions

    /**
     * If the game is in a state of indeterminacy, this method will return an action according to the
     * distribution of probabilities, or hidden information. If the game is in a definitive state null
     * is returned.
     *
     * @return a possible action, which determines the game
     */
    A determineNextAction();

    /**
     * Does a given action.
     *
     * @param action - the action to take
     * @return a new copy of the game with the given action applied
     * @throws IllegalArgumentException - In the case of a non-existing action or null
     * @throws IllegalStateException    - If game over
     */
    TurnBasedGame<A, B> doAction(A action);

    /**
     * Progresses the game if it currently is in an indeterminant state.
     *
     * @return a new copy of the game with an action applied
     * @throws IllegalArgumentException - If the game is not in an non-deterministic state
     * @throws IllegalStateException    - If game over
     */
    default TurnBasedGame<A, B> doAction() {
        return doAction(determineNextAction());
    }

    /**
     * Returns the record of all previous actions and which player has done it.
     *
     * @return the record of all previous actions
     */
    List<ActionRecord<A>> getActionRecords();

    /**
     * Returns the last action and which player did the action. If no action was done yet null is
     * returned.
     *
     * @return the last action record
     */
    default ActionRecord<A> getPreviousActionRecord() {
        List<ActionRecord<A>> actionRecordList = getActionRecords();
        if (actionRecordList.isEmpty()) {
            return null;
        }
        return actionRecordList.get(getNumberOfActions() - 1);
    }

    /**
     * Returns only the last action, but not which player did the action. If no action was done yet
     * given null is returned.
     *
     * @return the last action
     */
    default A getPreviousAction() {
        ActionRecord<A> previousAction = getPreviousActionRecord();
        return previousAction == null ? null : previousAction.getAction();
    }

    /**
     * Returns how many actions were taken.
     *
     * @return how many actions were taken
     */
    default int getNumberOfActions() {
        return getActionRecords().size();
    }

    /**
     * Returns the action records concatenated as one string.
     *
     * @return concatenated action record string
     */
    default String getActionRecordString() {
        return ActionRecord.iterableToString(getActionRecords());
    }

    //endregion

}
