package at.ac.tuwien.ifs.sge.core.game;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;

import java.util.*;

/**
 * A realtime game.
 *
 * @param <A> - Type of Action
 * @param <B> - Type of Map
 */
public interface RealTimeGame<A, B> extends Game<A, B> {

    //region General

    /**
     * Initializes the game and returns the actions that have to be applied for every player
     * Defaults to null for games that dont need initialization.
     *
     * @return a map with the player id as key and the players initialization actions as value
     */
    default Map<Integer, List<A>> initialize() { return null; }

    /**
     * Starts the game and all associated threads
     *
     */
    void start();


    /**
     * Waits for the game to be in a state where isGameOver returns true.
     *
     * @throws InterruptedException when the waiting is interrupted
     */
    void waitForGameOver(long timeoutMs) throws InterruptedException;


    /**
     * Stops the game and all associated threads
     * All tasks are interrupted and the game stops processing actions
     */
    void stop();

    /**
     * Creates a copy of the game state
     *
     * @return a copy of the game state
     */
    RealTimeGame<A, B> copy();

    /**
     * Advances the game in time and chronologically applies all actions that would occur
     * during the specified time.
     * Do not use this method on a game that has been started, since such a game uses the
     * {@link GameClock} and system time to schedule actions.
     * Only use this method for simulation purposes in combination with {@link RealTimeGame#applyActionEvent(GameActionEvent)}.
     *
     * @param timeMs - the time to advance in milliseconds
     */
    void advance(long timeMs) throws ActionException;

    /**
     * Checks if the game is running
     * The game enters a running state after calling {@link RealTimeGame#start()}
     *
     * @return true when the game has been started and not stopped since
     */
    boolean isRunning();


    /**
     * Waits for the next game update and returns it
     *
     * @return the latest {@link GameUpdate}
     * @throws InterruptedException when the thread is interrupted
     */
    GameUpdate<A> readGameUpdate() throws InterruptedException;


    /**
     * Gets the {@link GameClock} of the game.
     *
     * @return the {@link GameClock} of the game
     */
    GameClock getGameClock();

    //endregion

    //region Actions

    /**
     * Schedules an action to be performed at the execution time specified in the event
     * Events with an execution time that is earlier as the system time are discarded
     *
     * @param action - the action to enqueue
     */
    void scheduleActionEvent(GameActionEvent<A> action);


    /**
     * Checks if the action is a valid action of the player specified in the {@link GameActionEvent}
     * and applies it to the game returning a list of {@link GameUpdate} that are a consequence of the applied action.
     * If the source of the {@link GameActionEvent} is not from a player, the playerId of the {@link GameActionEvent}
     * has to be set to {@link GameActionEvent#INTERNAL_EVENT_ID} for it to be considered valid.
     *
     * @param actionEvent - the action event to apply
     * @return a list of {@link GameUpdate} with the applied action.
     */
    List<GameUpdate<A>> applyActionEvent(GameActionEvent<A> actionEvent);


    /**
     * Applies an action to the game and advances the game clock to the specified execution time
     *
     * @param action - the action to apply
     * @param executionTimeMs - the game time at which the action is performed
     * @throws ActionException when the action can not be applied
     * @return true if the action could be applied
     */
    ActionResult applyAction(A action, long executionTimeMs) throws ActionException;


    /**
     * Collects all possible moves of a specific player and returns them as a set. Should the game be over
     * an empty set is returned instead.
     *
     * @param playerId the id of the player
     * @return a set of this players possible moves
     */
    Set<A> getPossibleActions(int playerId);

    /**
     * Checks whether an action for a specific player is valid.
     *
     * @param action - the action
     * @param playerId - the id of the player performing the action.
     * @return true - if the action is valid and possible
     */
    default boolean isValidAction(A action, int playerId) {
        return getPossibleActions(playerId).contains(action);
    }

    /**
     * Returns the record of all previous actions.
     *
     * @return the record of all previous actions
     */
    List<TimedActionRecord<A>> getActionRecords();

    /**
     * Returns the last action record. If no action was done yet null is
     * returned.
     *
     * @return the last action record
     */
    default TimedActionRecord<A> getPreviousActionRecord() {
        List<TimedActionRecord<A>> actionRecordList = getActionRecords();
        if (actionRecordList.isEmpty()) {
            return null;
        }
        return actionRecordList.get(getNumberOfActions() - 1);
    }

    /**
     * Gets the last action.
     *
     * @return the last action
     */
    default A getPreviousAction() {
        TimedActionRecord<A> previousAction = getPreviousActionRecord();
        return previousAction == null ? null : previousAction.getAction();
    }

    /**
     * Returns how many actions were taken.
     *
     * @return how many actions were taken
     */
    default int getNumberOfActions() {
        return getActionRecords().size();
    }


    /**
     * Returns the action records concatenated as one string.
     *
     * @return concatenated action record string
     */
    default String getActionRecordString() {
        return TimedActionRecord.iterableToTimedString(getActionRecords());
    }

    //endregion
}
