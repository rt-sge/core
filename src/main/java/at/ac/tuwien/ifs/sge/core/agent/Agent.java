package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.engine.communication.AgentConnection;
import at.ac.tuwien.ifs.sge.core.security.PrivateKeyCertificatePair;
import at.ac.tuwien.ifs.sge.core.security.SecurityUtil;
import at.ac.tuwien.ifs.sge.core.util.Util;

import java.io.File;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;

public class Agent extends AgentInfo {

    static public final String SERVER_CERTIFICATE_KEY = "SERVER_CERTIFICATE";
    static public final String AGENT_PRIVATE_KEY = "AGENT_PRIVATE_KEY";
    static public final String AGENT_CERTIFICATE_KEY = "AGENT_CERTIFICATE_KEY";

    static public final String DEFAULT_MIN_HEAP_SIZE = "256M";
    static public final String DEFAULT_MAX_HEAP_SIZE = "2G";
    static public final String DEFAULT_THREAD_STACK_SIZE = "1024k";

    static protected final String JAR_COMMAND = "-jar";
    static protected final String CLASSPATH_COMMAND = "-cp";

    static private final String JAVA_COMMAND = "java";
    static private final String MIN_HEAP_SIZE_COMMAND = "-Xms";
    static private final String MAX_HEAP_SIZE_COMMAND = "-Xmx";
    static private final String MAX_STACK_SIZE_COMMAND = "-Xss";
    static private int AGENT_COUNT = 0;


    protected Integer agentId;

    protected MemorySpecifications memorySpecifications;
    protected boolean createLogFile;
    protected Process process;
    protected AgentConnection connection;
    protected boolean setUp;
    protected boolean initialized;


    protected boolean shutDown;
    protected boolean disqualified;


    public Agent(Integer agentId,
                 MemorySpecifications memorySpecifications,
                 boolean createLogFile,
                 String name,
                 File path,
                 boolean realtime) {
        super(name, path, realtime);
        this.agentId = agentId;
        this.memorySpecifications = memorySpecifications;
        this.createLogFile = createLogFile;
    }

    public Process start(Integer playerId,
                         String gameFilePath,
                         String gameClassName,
                         X509Certificate serverCertificate,
                         PrivateKeyCertificatePair agentPrivateKeyCertificatePair) throws IOException, CertificateEncodingException {
        return start(List.of(JAR_COMMAND, path.getAbsolutePath()), playerId, gameFilePath, gameClassName, serverCertificate, agentPrivateKeyCertificatePair);
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setMemorySpecifications(MemorySpecifications memorySpecifications) {
        this.memorySpecifications = memorySpecifications;
    }

    public void setCreateLogFile(boolean createLogFile) {
        this.createLogFile = createLogFile;
    }

    public Process getProcess() {
        return process;
    }

    public void setConnection(AgentConnection connection) {
        this.connection = connection;
    }

    public AgentConnection getConnection() {
        return connection;
    }

    public Boolean isClaimed() {
        return connection != null;
    }

    public boolean isSetUp() {
        return setUp;
    }

    public void setSetUp(boolean setUp) {
        this.setUp = setUp;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    public boolean isShutDown() {
        return shutDown;
    }

    public void setShutDown(boolean shutDown) {
        this.shutDown = shutDown;
    }

    public boolean isDisqualified() { return disqualified; }

    public void setDisqualified(boolean disqualified) {
        this.disqualified = disqualified;
    }

    public boolean shouldReceiveMessages() { return !disqualified && connection != null && !connection.isClosed();}

    public void reset() {
        this.process = null;
        this.connection = null;
        this.setUp = false;
        this.initialized = false;
        this.shutDown = false;
        this.disqualified = false;
    }

    public boolean isReadyForMatch() {
        return process == null
                && connection == null
                && !setUp;
    }

    @Override
    public String toString() {
        return agentId.toString() + " " + this.name;
    }

    protected String getLogFilename() {
        var now = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss").format(new Date());
        return now + "-" + agentId.toString() + "_" + this.name + "_log.txt";
    }

    protected Process start(Collection<String> command,
                            Integer playerId,
                            String gameFilePath,
                            String gameClassName,
                            X509Certificate serverCertificate,
                            PrivateKeyCertificatePair agentPrivateKeyCertificatePair) throws IOException, CertificateEncodingException {
        if (memorySpecifications == null)
            memorySpecifications = new MemorySpecifications(DEFAULT_MIN_HEAP_SIZE, DEFAULT_MAX_HEAP_SIZE, DEFAULT_THREAD_STACK_SIZE);

        var startupCmd = new ArrayList<>(List.of(
                JAVA_COMMAND,
                MIN_HEAP_SIZE_COMMAND + memorySpecifications.getMinHeapSize(),
                MAX_HEAP_SIZE_COMMAND + memorySpecifications.getMaxHeapSize(),
                MAX_STACK_SIZE_COMMAND + memorySpecifications.getThreadStackSize(),
                "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:" + (8808 + AGENT_COUNT++)));
        startupCmd.addAll(command);
        startupCmd.addAll(List.of(
                playerId.toString(),
                toString(),
                gameFilePath,
                gameClassName
        ));
        var processBuilder = new ProcessBuilder(startupCmd);
        var env = processBuilder.environment();
        env.put(SERVER_CERTIFICATE_KEY, SecurityUtil.x509CertToPem(serverCertificate));
        env.put(AGENT_PRIVATE_KEY, SecurityUtil.keyToBase64String(agentPrivateKeyCertificatePair.getPrivateKey()));
        env.put(AGENT_CERTIFICATE_KEY, SecurityUtil.x509CertToPem(agentPrivateKeyCertificatePair.getCertificate()));

        try {
            var factory = KeyFactory.getInstance("Ed25519", "BC");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (createLogFile)
            redirectOutputToLogFile(processBuilder);

        process = processBuilder.start();
        return process;
    }

    private void redirectOutputToLogFile(ProcessBuilder processBuilder) throws IOException {
        var dirFile = Util.createLogDirectory();
        var logFile = new File(dirFile.getPath() + "/" + getLogFilename());
        if (logFile.createNewFile()) {
            processBuilder.redirectErrorStream(true);
            processBuilder.redirectOutput(ProcessBuilder.Redirect.appendTo(logFile));
        } else throw new IOException("Could not create log file for agent " + this);
    }

    public String redirectOutputToLogFile() throws IOException {
        var dirFile = Util.createLogDirectory();
        return dirFile.getPath() + "/" + getLogFilename();
    }
}
