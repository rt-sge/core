package at.ac.tuwien.ifs.sge.core.game;

public class StringGameConfiguration implements GameConfiguration {

    private final String configuration;

    public StringGameConfiguration(String configuration) {
        this.configuration = configuration;
    }

    @Override
    public GameConfiguration getConfigurationForPlayer(int playerId) {
        return new StringGameConfiguration(configuration);
    }

    public String getConfiguration() {
        return configuration;
    }
}
