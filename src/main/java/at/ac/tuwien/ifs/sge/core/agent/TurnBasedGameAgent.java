package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.game.Game;

import java.util.concurrent.TimeUnit;

public interface TurnBasedGameAgent<G extends Game<A, ?>, A> extends GameAgent<G, A> {

    /**
     * Called when it is the agents turn to compute the next action.
     *
     * @param game the current game state
     * @param computationTime the time the agent is allowed to use for the computation
     * @param timeUnit the time unit of the computation time
     * @return the action the agent wants to play
     */
    A computeNextAction(G game, long computationTime, TimeUnit timeUnit);

}
