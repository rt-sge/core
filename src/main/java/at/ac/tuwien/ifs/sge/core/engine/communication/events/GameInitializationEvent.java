package at.ac.tuwien.ifs.sge.core.engine.communication.events;

import java.util.List;

public class GameInitializationEvent<A> extends SgeEvent {

    private final List<A> initializationActions;
    private final long startTimeMs;



    public GameInitializationEvent(int playerId, List<A> initializationActions, long startTimeMs) {
        super(playerId);
        this.initializationActions = initializationActions;
        this.startTimeMs = startTimeMs;
    }

    public List<A> getInitializationActions() {
        return initializationActions;
    }

    public long getStartTimeMs() {
        return startTimeMs;
    }
}
