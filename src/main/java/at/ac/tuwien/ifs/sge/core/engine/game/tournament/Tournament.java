package at.ac.tuwien.ifs.sge.core.engine.game.tournament;

import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.security.cert.CertificateEncodingException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public interface Tournament extends
    Callable<List<MatchResult>> {

  @Override
  List<MatchResult> call() throws ExecutionException, InterruptedException, CertificateEncodingException;

  PublishSubject<Match<?, ?>> getNextMatchSubject();
  PublishSubject<MatchResult> getNextMatchResultSubject();

  List<Agent> getAgents();

  int getNrOfPlayersPerMatch();

  TournamentMode getMode();

  String toTextRepresentation();

}
