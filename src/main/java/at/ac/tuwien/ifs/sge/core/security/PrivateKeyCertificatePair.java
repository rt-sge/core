package at.ac.tuwien.ifs.sge.core.security;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class PrivateKeyCertificatePair {
    final private PrivateKey privateKey;
    final private X509Certificate certificate;

    public PrivateKeyCertificatePair(PrivateKey privateKey, X509Certificate certificate) {
        this.privateKey = privateKey;
        this.certificate = certificate;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public X509Certificate getCertificate() {
        return certificate;
    }
}
