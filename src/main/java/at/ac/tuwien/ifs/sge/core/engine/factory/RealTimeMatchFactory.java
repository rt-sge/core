package at.ac.tuwien.ifs.sge.core.engine.factory;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.engine.game.match.RealTimeMatch;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;

public class RealTimeMatchFactory extends MatchFactory {

    private final Logger log;
    private final ExecutorService threadPool;
    private final long timeLimitMs;
    private final boolean showTextRepresentation;

    public RealTimeMatchFactory(GameFactory<?> gameFactory,
                                GameConfiguration gameConfiguration,
                                int nrOfPlayers,
                                Logger log,
                                ExecutorService threadPool,
                                long timeLimitMs,
                                boolean showTextRepresentation) {
        super(gameFactory, gameConfiguration, nrOfPlayers);
        this.log = log;
        this.threadPool = threadPool;
        this.timeLimitMs = timeLimitMs;
        this.showTextRepresentation = showTextRepresentation;
    }

    @Override
    public Match<?, ?> newMatch(HashMap<Integer, Agent> agentMap) {
        var game = newGameInstance();
        return new RealTimeMatch<>((RealTimeGame<?, ?>) game, agentMap, log, threadPool, timeLimitMs, showTextRepresentation);
    }

}
