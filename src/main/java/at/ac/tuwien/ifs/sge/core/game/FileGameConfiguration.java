package at.ac.tuwien.ifs.sge.core.game;

import java.io.File;

public class FileGameConfiguration implements GameConfiguration {

    private final File file;

    public FileGameConfiguration(File file) {
        this.file = file;
    }

    @Override
    public GameConfiguration getConfigurationForPlayer(int playerId) {
        return new FileGameConfiguration(file);
    }

    public File getFile() {
        return file;
    }
}
