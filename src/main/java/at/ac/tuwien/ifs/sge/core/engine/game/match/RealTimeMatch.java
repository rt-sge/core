package at.ac.tuwien.ifs.sge.core.engine.game.match;

import at.ac.tuwien.ifs.sge.core.engine.communication.AgentMessage;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameInitializationEvent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.StartGameEvent;
import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;
import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;

import java.security.cert.CertificateEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;

public class RealTimeMatch<G extends RealTimeGame<A, ?>, A> extends
        Match<G, A> {

    public static final int INITIALIZE_TIMEOUT_SECONDS = 5;

    private final G game;
    private final long timeLimitMs;

    private Future<?> agentActionsThreadFuture;
    private Future<?> gameUpdateThreadFuture;

    private final CountDownLatch communicationShutdownLatch = new CountDownLatch(2);

    public RealTimeMatch(G game,
                         HashMap<Integer, Agent> gameAgents,
                         Logger log,
                         ExecutorService pool,
                         long timeLimitMs,
                         boolean showTextRepresentation) {
        super(gameAgents, log, pool, showTextRepresentation);
        this.game = game;
        this.timeLimitMs = timeLimitMs;
        if (game.getNumberOfPlayers() != gameAgents.size()) {
            throw new IllegalArgumentException("Not the correct number of players");
        }
    }

    @Override
    public MatchResult call() throws InterruptedException, ExecutionException, CertificateEncodingException {

        if (matchResult != null) {
            return matchResult;
        }

        log.info("Starting match...");

        var startTime = System.nanoTime();
        var endTime = System.nanoTime();

        try {
            startServer();

            startAgents();

            setUpAgents();

            initializeGame();

            if (atLeastTwoPlayersNotDisqualified()) {

                game.start();

                startAgentActionsThread();
                startGameUpdateThread();

                startGame();

                startTime = System.nanoTime();

                timeLimitMsSubject.onNext(timeLimitMs);
                game.waitForGameOver(timeLimitMs);

                endTime = System.nanoTime();

                game.stop();

                stopCommunicationThreads();

                communicationShutdownLatch.await();
            }

            tearDownAgents();

            printGameOver(false);

            matchResult = new MatchResult(gameAgents, startTime, endTime, calculateGameResult());
            return matchResult;
        } catch (InterruptedException | CertificateEncodingException | ExecutionException e ) {
            endTime = System.nanoTime();
            log.info("Game interrupted!");
            log.debugPrintStackTrace(e);
            if (game.isRunning())
                game.stop();
            stopCommunicationThreads();
            if (playersClaimed)
                tearDownAgents();
            stopMatch(startTime, endTime, false);

            throw e;

        } finally {
            server.stop();
            terminateAgentProcesses();
            removeAgentShutdownHook();
            resetAgents();
        }
    }

    @Override
    public G getGame() {
        return game;
    }



    private void stopCommunicationThreads() {
        if (agentActionsThreadFuture != null)
            agentActionsThreadFuture.cancel(true);
        if (gameUpdateThreadFuture != null)
            gameUpdateThreadFuture.cancel(true);
    }

    private void startGame() {
        sendToEveryAgent(new StartGameEvent());
    }


    @SuppressWarnings("unchecked")
    private void startAgentActionsThread() {
        final Runnable agentActionsRunnable = () -> {
            for (;;) {
                try {
                    var message = server.readMessage();
                    var content = message.getContent();
                    if (content instanceof GameActionEvent) {
                        var actionEvent = (GameActionEvent<A>) content;
                        var action = actionEvent.getAction();
                        log.debug("Action received: " + action);
                        game.scheduleActionEvent(actionEvent);
                    }
                } catch (InterruptedException e) {
                    log.debug("Shutting down agent actions task...");
                    break;
                } catch (Exception e) {
                    log.error("Exception in agent actions thread: " + e);
                    log.printStackTrace(e);
                }
            }
            communicationShutdownLatch.countDown();
        };
        agentActionsThreadFuture = pool.submit(agentActionsRunnable);
    }

    private void startGameUpdateThread() {
        final Runnable gameUpdateRunnable = () -> {
            for (;;) {
                try {
                    var gameUpdate = game.readGameUpdate();
                    //log.info("Game update: " + gameUpdate.getAction() + " for " + Arrays.toString(gameUpdate.getPlayersToUpdate().toArray()) );
                    updateAgents(gameUpdate);
                    if (showTextRepresentation)
                        log._inf_(game.toTextRepresentation());
                } catch (InterruptedException e) {
                    log.debug("Shutting down game update task...");
                    break;
                } catch (Exception e) {
                    log.error("Exception in game update thread: " + e);
                    log.printStackTrace(e);
                }
            }
            communicationShutdownLatch.countDown();
        };
        gameUpdateThreadFuture = pool.submit(gameUpdateRunnable);
    }

    @SuppressWarnings("unchecked")
    private void initializeGame() throws InterruptedException, ExecutionException {
        var initializationActions = game.initialize();
        if (initializationActions != null) {
            var nrOfAgents = gameAgents.size() - getNrOfDisqualifiedAgents();
            log.traProcess_("Initializing agent games", 0, nrOfAgents);

            Runnable initializeAgentsRunnable = () -> {
                var initializedAgents = 0;
                do {
                    AgentMessage message;
                    try {
                        message = server.readMessage();
                    } catch (InterruptedException e) {
                        break;
                    }
                    var content = message.getContent();
                    if (content instanceof GameInitializationEvent) {
                        var initEventAck = (GameInitializationEvent<A>) content;
                        var agent = gameAgents.get(initEventAck.getPlayerId());
                        if (!agent.isInitialized()) {
                            agent.setSetUp(true);
                            initializedAgents++;
                            log._tra_("\r");
                            log.traProcess_("Initializing agent games", initializedAgents, nrOfAgents);
                        }
                    }
                } while (initializedAgents != nrOfAgents);
            };

            var initializeAgentsFuture = pool.submit(initializeAgentsRunnable);

           for (var playerId : gameAgents.keySet()) {
               var agent = gameAgents.get(playerId);
               var initActions = List.<A>of();
               if (initializationActions.containsKey(playerId))
                   initActions = initializationActions.get(playerId);
               var initEvent = new GameInitializationEvent<>(playerId, initActions, game.getGameClock().getStartTimeMs());
               sendToAgent(agent, initEvent);
           }

            try {
                initializeAgentsFuture.get(INITIALIZE_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                log._trace_(", done.");
            } catch (InterruptedException e) {
                log._trace_(", canceled.");
                throw new InterruptedException("Interrupted while initializing agents.");
            } catch (ExecutionException e) {
                log._trace_(", canceled.");
                throw new ExecutionException("Exception while waiting for agents to initialize.", e);
            } catch (TimeoutException e) {
                initializeAgentsFuture.cancel(true);
                log._trace_(", canceled.");
                for (var entry : gameAgents.entrySet()) {
                    var agent = entry.getValue();
                    if (!agent.isInitialized()) {
                        agent.setDisqualified(true);
                    }
                }
            }
        }
    }
}
