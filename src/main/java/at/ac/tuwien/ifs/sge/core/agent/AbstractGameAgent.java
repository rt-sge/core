package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.communication.*;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.*;
import at.ac.tuwien.ifs.sge.core.engine.factory.GameFactory;
import at.ac.tuwien.ifs.sge.core.engine.loader.GameLoader;
import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static java.lang.Integer.parseInt;

public abstract class AbstractGameAgent<G extends Game<A, ?>, A> implements GameAgent<G, A> {

    public static final int PLAYER_ID_INDEX = 0;
    public static final int PLAYER_NAME_INDEX = 1;
    public static final int GAME_URL_INDEX = 2;
    public static final int GAME_CLASS_NAME_INDEX = 3;

    public static int getPlayerIdFromArgs(String[] args) {
        var id = -1;
        try {
            id = parseInt(args[PLAYER_ID_INDEX]);
        } catch (NumberFormatException e) {
            System.err.println("[agent error] could not parse player id from arguments!");
        }
        return id;
    }

    public static String getPlayerNameFromArgs(String[] args) {
        return args[PLAYER_NAME_INDEX];
    }


    public static Class<?> getGameClassFromArgs(String[] args) {
        Class<?> clazz = null;
        try {
            var gameUrl = new URL(args[GAME_URL_INDEX]);
            var classLoader = new URLClassLoader(new URL[]{gameUrl});
            clazz = classLoader
                    .loadClass(args[GAME_CLASS_NAME_INDEX]);

        } catch (MalformedURLException e) {
            System.err.println("[agent error] could not parse game file url from arguments!");
        } catch (ClassNotFoundException e) {
            System.err.println("[agent error] game class specified in arguments was not found!");
        }
        return clazz;
    }

    private static final int MIN_NR_OF_THREADS = ServerConnection.NR_OF_TASKS;

    protected ServerConnection serverConnection;
    protected ExecutorService pool;
    protected GameFactory<G> gameFactory;
    protected GameLoader<G> gameLoader;

    protected Logger log;
    protected Comparator<Game<A, ?>> gameUtilityComparator;
    protected Comparator<Game<A, ?>> gameHeuristicComparator;
    protected Comparator<Game<A, ?>> gameComparator;
    protected double[] minMaxWeights;
    protected double[] evenWeights;
    protected int playerId;
    protected String playerName;
    protected int numberOfPlayers;

    private final List<Future<?>> serverFutures = new ArrayList<>();


    protected AbstractGameAgent(Class<G> gameClass, int playerId, String playerName) { this(gameClass, playerId, playerName, 0); }

    protected AbstractGameAgent(Class<G> gameClass, int playerId, String playerName, int logLevel) {
        this.playerId = playerId;
        this.playerName = playerName;

        log = new Logger(logLevel, "[sge-agent ", "",
                "trace]: ", System.out, "",
                "debug]: ", System.out, "",
                "info]: ", System.out, "",
                "warn]: ", System.err, "",
                "error]: ", System.err, "");


        gameLoader = new GameLoader<>(gameClass, log);

        initializeThreadPool();

        setupServerConnection();

        log.debug("Initialized with name '" + playerName + "' and player id '" + playerId + "'");
    }


    public void start() {
        var claim = new PlayerClaimEvent(playerId, playerName);
        serverConnection.sendMessage(claim);
        var agentSetup = false;
        while (!agentSetup) {
            try {
                var event = serverConnection.readMessage();
                if (handleEvent(event)) continue;
                if (event instanceof SetUpEvent setUpEvent) {
                    setup(setUpEvent.getGameConfiguration(), setUpEvent.getNumberOfPlayers());
                    var setUpAckEvent = new SetUpEvent();
                    setUpAckEvent.setPlayerId(playerId);
                    serverConnection.sendMessage(setUpAckEvent);
                    agentSetup = true;
                }
            } catch (InterruptedException e) {
                break;
            }
        }
    }



    @Override
    public void setup(GameConfiguration gameConfiguration, int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
        minMaxWeights = new double[numberOfPlayers];
        Arrays.fill(minMaxWeights, -1D);
        minMaxWeights[playerId] = 1D;

        evenWeights = new double[numberOfPlayers];
        Arrays.fill(evenWeights, -1D / (-1D + numberOfPlayers));
        evenWeights[playerId] = 1D;

        gameUtilityComparator = Comparator
                .comparingDouble(o -> o.getUtilityValue(minMaxWeights));
        gameHeuristicComparator = Comparator
                .comparingDouble(o -> o.getHeuristicValue(minMaxWeights));
        gameComparator = gameUtilityComparator.thenComparing(gameHeuristicComparator);

        log.debug("Set up with a total of " + numberOfPlayers + " players");
    }


    protected boolean handleEvent(SgeEvent event) {
        if (event == null || event.getPlayerId() != playerId) {
            return true;
        }
        if (event instanceof TearDownEvent) {
            shutdown();
            var tearDownAckEvent = new TearDownEvent(playerId);
            try {
                serverConnection.sendMessageAndWait(tearDownAckEvent, 1000);
            } catch (InterruptedException ignored) { }
            log.debug("match is over, shutting down agent...");
            serverConnection.close();
            shutdownThreadPool();
            System.exit(0);
            return true;
        }
        return false;
    }

    abstract protected void initializeThreadPool();

    protected int getMinimumNumberOfThreads() {
        return MIN_NR_OF_THREADS;
    }

    private void setupServerConnection() {
        if (pool == null) {
            log.error("Error while connecting to server: executor service not initialized!");
            return;
        }
        serverConnection = new ServerConnection(log, gameLoader.getClassLoader());
        var tasks = serverConnection.getTasks();
        for (var task : tasks)
            serverFutures.add(pool.submit(task));
    }

    private void shutdownThreadPool() {
        serverFutures.forEach(f -> f.cancel(true));
        pool.shutdown();
    }
}
