package at.ac.tuwien.ifs.sge.core.engine.game.tournament;

import at.ac.tuwien.ifs.sge.core.engine.factory.MatchFactory;
import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;

import java.util.List;

public enum TournamentMode {

  ROUND_ROBIN {
    @Override
    public Tournament getTournament(
        MatchFactory matchFactory,
        List<Agent> gameAgents,
        int nrOfPlayersPerMatch,
        Logger log) {
      return new RoundRobin(
              matchFactory,
              gameAgents,
              nrOfPlayersPerMatch,
              log);
    }

    @Override
    public int getMinimumPerRound() {
      return 2;
    }

    @Override
    public int getMaximumPerRound() {
      return Integer.MAX_VALUE;
    }


  },

  DOUBLE_ROUND_ROBIN {
    @Override
    public Tournament getTournament(
        MatchFactory matchFactory,
        List<Agent> gameAgents,
        int nrOfPlayersPerMatch,
        Logger log) {
      return new DoubleRoundRobin(
              matchFactory,
              gameAgents,
              nrOfPlayersPerMatch,
              log);
    }

    @Override
    public int getMinimumPerRound() {
      return 2;
    }

    @Override
    public int getMaximumPerRound() {
      return Integer.MAX_VALUE;
    }
  };

  public abstract Tournament getTournament(
      MatchFactory matchFactory,
      List<Agent> gameAgents,
      int nrOfPlayersPerMatch,
      Logger log);

  public abstract int getMinimumPerRound();

  public abstract int getMaximumPerRound();
}
