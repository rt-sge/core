package at.ac.tuwien.ifs.sge.core.game.exception;

public class ActionException extends Exception {
    public ActionException(String message, Exception cause) {
        super(message, cause);
    }
}
