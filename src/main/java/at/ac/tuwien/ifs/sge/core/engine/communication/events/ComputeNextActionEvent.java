package at.ac.tuwien.ifs.sge.core.engine.communication.events;

import at.ac.tuwien.ifs.sge.core.game.TurnBasedGame;

import java.util.concurrent.TimeUnit;

public class ComputeNextActionEvent<G extends TurnBasedGame<?, ?>> extends SgeEvent {

    private final long computationTime;
    private final TimeUnit timeUnit;

    private final G game;

    public ComputeNextActionEvent(G game, long computationTime, TimeUnit timeUnit) {
        this.game = game;
        this.computationTime = computationTime;
        this.timeUnit = timeUnit;
    }

    public ComputeNextActionEvent(Integer playerId, G game, long computationTime, TimeUnit timeUnit) {
        super(playerId);
        this.game = game;
        this.computationTime = computationTime;
        this.timeUnit = timeUnit;
    }

    public G getGame() {
        return game;
    }

    public long getComputationTime() {
        return computationTime;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }
}
