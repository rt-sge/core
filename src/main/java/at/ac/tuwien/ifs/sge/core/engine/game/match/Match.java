package at.ac.tuwien.ifs.sge.core.engine.game.match;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.communication.AgentMessage;
import at.ac.tuwien.ifs.sge.core.engine.communication.EngineServer;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.*;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.*;
import io.reactivex.rxjava3.subjects.ReplaySubject;

import java.io.IOException;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

public abstract class Match<G extends Game<A, ?>, A> implements Callable<MatchResult> {

    public static final int PLAYER_CLAIM_TIMEOUT_SECONDS = 5;
    public static final int SET_UP_TIMEOUT_SECONDS = 5;
    public static final int TEAR_DOWN_TIMEOUT_SECONDS = 5;

    public static Match<?, ?> getMatchForGame(Game<?, ?> game,
                                              HashMap<Integer, Agent> playerMap,
                                              long computationTime,
                                              TimeUnit timeUnit,
                                              Logger log,
                                              ExecutorService pool,
                                              int maxActions,
                                              long timeLimitMs,
                                              boolean showTextRepresentation) {
        if (game.isRealtime()) {
            return new RealTimeMatch<>(
                    (RealTimeGame<?, ?>) game,
                    playerMap, log, pool, timeLimitMs, showTextRepresentation);
        } else {
            return new TurnBasedMatch<>(
                    (TurnBasedGame<?, ?>) game,
                    playerMap, computationTime, timeUnit, log, pool, maxActions, showTextRepresentation);
        }
    }

    public ReplaySubject<Agent> agentStartedSubject = ReplaySubject.create();
    public ReplaySubject<Long> timeLimitMsSubject = ReplaySubject.create();


    protected final HashMap<Integer, Agent> gameAgents;
    protected final Logger log;
    protected final ExecutorService pool;
    protected final boolean showTextRepresentation;

    protected EngineServer server;
    protected Future<?> serverFuture;
    protected MatchResult matchResult;
    protected boolean playersClaimed;

    private final Thread agentShutdownHook = new Thread(this::terminateAgentProcesses);

    public Match(HashMap<Integer, Agent> gameAgents,
                 Logger log,
                 ExecutorService pool,
                 boolean showTextRepresentation) {
        this.gameAgents = gameAgents;
        this.log = log;
        this.pool = pool;
        this.showTextRepresentation = showTextRepresentation;

        this.matchResult = null;
    }

    @Override
    abstract public MatchResult call() throws InterruptedException, ExecutionException, CertificateEncodingException;

    abstract public G getGame();

    public HashMap<Integer, Agent> getGameAgents() {
        return gameAgents;
    }

    protected void stopMatch(long startTime, long endTime, boolean printActionRecords) {
        log.info("Stopping match...");
        matchResult = new MatchResult(gameAgents, startTime, System.nanoTime(), getUndecidedResult());
        log._info_(getGame().toTextRepresentation());
        if (printActionRecords) {
            log.info(getGame().getNumberOfActions() + " actions: ");
            log._info(getGame().getActionRecordString());
        }
    }

    protected void printGameOver(boolean printActionRecords) {
        log._info_("-----");
        log.info("Game over.");
        log._info_(getGame().toTextRepresentation());
        if (printActionRecords) {
            log.info(getGame().getNumberOfActions() + " actions: ");
            log._info(getGame().getActionRecordString());
        }
    }


    protected void setUpAgents() throws ExecutionException, InterruptedException {
        var nrOfAgents = gameAgents.size() - getNrOfDisqualifiedAgents();
        log.traProcess_("Setting up agents", 0, nrOfAgents);

        Runnable setUpAgentsRunnable = () -> {
            var setupAgents = 0;
            do {
                AgentMessage message;
                try {
                    message = server.readMessage();
                } catch (InterruptedException e) {
                    break;
                }
                var content = message.getContent();
                if (content instanceof SetUpEvent setUpEventAck) {
                    var agent = gameAgents.get(setUpEventAck.getPlayerId());
                    if (!agent.isSetUp()) {
                        agent.setSetUp(true);
                        setupAgents++;
                        log._tra_("\r");
                        log.traProcess_("Setting up agents", setupAgents, nrOfAgents);
                    }
                }
            } while (setupAgents != nrOfAgents);
        };

        var setUpAgentsFuture = pool.submit(setUpAgentsRunnable);

        var gameConfiguration = getGame().getGameConfiguration();
        if (!getGame().hasDivergingGameConfiguration()) {
            var setUpEvent = new SetUpEvent(gameConfiguration, gameAgents.size());
            sendToEveryAgent(setUpEvent);
        } else {
            var setUpEvent = new SetUpEvent(gameAgents.size());
            sendToEveryAgent(setUpEvent, (event, playerId) -> {
                event.setGameConfiguration(gameConfiguration.getConfigurationForPlayer(playerId));
                return event;
            });
        }

        try {
            setUpAgentsFuture.get(SET_UP_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            log._trace_(", done.");
        } catch (InterruptedException e) {
            log._trace_(", canceled.");
            throw new InterruptedException("Interrupted while setting up agents.");
        } catch (ExecutionException e) {
            log._trace_(", canceled.");
            throw new ExecutionException("Exception while waiting for agents to set up.", e);
        } catch (TimeoutException e) {
            setUpAgentsFuture.cancel(true);
            log._trace_(", canceled.");
            for (var entry : gameAgents.entrySet()) {
                var agent = entry.getValue();
                if (!agent.isSetUp()) {
                    agent.setDisqualified(true);
                }
            }
        }

    }

    protected void tearDownAgents() {
        log.traProcess_("Tearing down agents", 0, gameAgents.size());

        Runnable tearDownAgentsRunnable = () -> {
            var tearedDownAgents = 0;
            do {
                AgentMessage message;
                try {
                    message = server.readMessage();
                } catch (InterruptedException e) {
                    break;
                }
                var content = message.getContent();
                if (content instanceof TearDownEvent tearDownEventAck) {
                    var agent = gameAgents.get(tearDownEventAck.getPlayerId());
                    if (!agent.isShutDown()) {
                        agent.setShutDown(true);
                        tearedDownAgents++;
                        log._tra_("\r");
                        log.traProcess_("Tearing down agents", tearedDownAgents, gameAgents.size());
                    }
                }
            } while (tearedDownAgents != gameAgents.size());

        };

        var tearDownAgentsFuture = pool.submit(tearDownAgentsRunnable);

        var tearDownEvent = new TearDownEvent();
        sendToEveryAgent(tearDownEvent);

        try {
            tearDownAgentsFuture.get(TEAR_DOWN_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            log._trace_(", done.");
        } catch (InterruptedException e) {
            log._trace_(", canceled.");
            log.warn("Interrupted while tearing down agents.");
        } catch (ExecutionException e) {
            log._trace_(", canceled.");
            log.warn("Exception while waiting for agents to shut down.");
            log.printStackTrace(e);
        } catch (TimeoutException e) {
            log._trace_(", canceled.");
            tearDownAgentsFuture.cancel(true);
            log.warn("Agent tear down timed out.");
        }

    }


    protected <T extends SgeEvent> void sendToEveryAgent(T event) {
        sendToEveryAgent(event, null);
    }

    protected <T extends SgeEvent> void sendToEveryAgent(T event, BiFunction<T, Integer, T> applyToEvent) {
        for (var entry : gameAgents.entrySet()) {
            var agent = entry.getValue();
            if (!agent.shouldReceiveMessages()) continue;
            var connection = agent.getConnection();
            event.setPlayerId(entry.getKey());
            if (applyToEvent != null) {
                event = applyToEvent.apply(event, entry.getKey());
            }
            connection.sendMessage(event);
        }
    }

    protected void sendToAgents(SgeEvent event, List<Integer> playerIds) {
        for (var id : playerIds) {
            var agent = gameAgents.get(id);
            if (!agent.shouldReceiveMessages()) continue;
            var connection = agent.getConnection();
            event.setPlayerId(id);
            connection.sendMessage(event);
        }
    }

    protected void sendToAgent(Agent agent, SgeEvent event) {
        if (!agent.shouldReceiveMessages()) return;
        var connection = agent.getConnection();
        connection.sendMessage(event);
    }

    protected void updateAgents(GameUpdate<A> update) {
        SgeEvent updateGameEvent;
        var playerId = update.getPlayer();
        if (update instanceof InvalidGameActionUpdate<A> invalid) {
            updateGameEvent = new InvalidGameActionEvent<>(playerId, update.getActions(), update.getExecutionTimeMs(), invalid.getErrorMessage());
        } else {
            updateGameEvent = new GameUpdateEvent<>(playerId, update.getActions(), update.getExecutionTimeMs());
        }
        var agent = gameAgents.get(playerId);
        sendToAgent(agent, updateGameEvent);
    }


    protected void startServer() {
        var classLoader = getGame().getClass().getClassLoader();
        server = new EngineServer(0, log, pool, classLoader, gameAgents.size());
        serverFuture = pool.submit(server);
    }

    protected void startAgents() throws InterruptedException, ExecutionException, CertificateEncodingException {
        log.traProcess_("Starting agents: ", 0, gameAgents.size());
        AtomicInteger startedAgents = new AtomicInteger();

        // make sure that all agent processes are terminated when the engines jvm gets shutdown unexpectedly
        Runtime.getRuntime().addShutdownHook(agentShutdownHook);

        for (var entry : gameAgents.entrySet()) {
            var playerId = entry.getKey();
            var agent = entry.getValue();

            Runnable listenForPlayerClaim = () -> {
                do {
                    try {
                        AgentMessage message;
                        message = server.readMessage();
                        var content = message.getContent();
                        if (content instanceof PlayerClaimEvent) {
                            var claim = (PlayerClaimEvent) content;
                            var connection = message.getConnection();
                            if (claim.getPlayerId() == playerId) {
                                agent.setConnection(connection);
                                startedAgents.getAndIncrement();
                                log._tra_("\r");
                                log.traProcess_("Starting agents: ", startedAgents.get(), gameAgents.size());
                            } else {
                                server.declinePlayerClaim(connection);
                            }
                        }
                    } catch (InterruptedException e) {
                        break;
                    }
                } while (!agent.isClaimed());
            };
            var listenForPlayerClaimFuture = pool.submit(listenForPlayerClaim);

            try {
                var process = agent.start(playerId,
                        getGameFileUrl(),
                        getGame().getClass().getName(),
                        server.getServerCertificate(),
                        server.popClientPrivateKeyCertificatePair());
                agentStartedSubject.onNext(agent);
            } catch (IOException e) {
                log.error("Could not start agent: " + agent + " | " + e);
                agent.setDisqualified(true);
            }

            try {
                listenForPlayerClaimFuture.get(PLAYER_CLAIM_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                throw new InterruptedException("Interrupted while waiting for player claim.");
            } catch (ExecutionException e) {
                throw new ExecutionException("Exception while waiting for player claim.", e);
            } catch (TimeoutException e) {
                log.warn("Player with id " + playerId + " was not claimed by agent " + agent + "!");
                agent.setDisqualified(true);
                listenForPlayerClaimFuture.cancel(true);
            }
        }
        log._trace_(", done.");
        playersClaimed = true;
        if (startedAgents.get() == gameAgents.size())
            log.debug("All agents started and all players claimed.");
        else
            log.debug("Some players have not been claimed!");

    }

    protected double[] getUndecidedResult() {
        var result = new double[gameAgents.size()];
        Arrays.fill(result, 0D);
        return result;
    }

    protected void resetAgents() {
        agentStartedSubject = ReplaySubject.create();
        for (var agent : gameAgents.values()) {
            agent.reset();
        }
    }

    protected void terminateAgentProcesses() {
        for (var agent : gameAgents.values()) {
            if (agent.getProcess() != null)
                agent.getProcess().destroy();
        }
    }

    protected void removeAgentShutdownHook() {
        Runtime.getRuntime().removeShutdownHook(agentShutdownHook);
    }

    protected double[] calculateGameResult() {
        var result = new double[gameAgents.size()];
        if (isEveryoneExceptOneDisqualified()) {
            gameAgents.entrySet().stream()
                    .filter(kv -> !kv.getValue().isDisqualified())
                    .findFirst().ifPresent(winner -> result[winner.getKey()] = 1D);
        } else {
            double[] utility = getGame().getGameUtilityValue();
            for (var id : gameAgents.keySet()) {
                var agent = gameAgents.get(id);
                if (agent.isDisqualified()) {
                    result[id] = -1D;
                } else {
                    result[id] = utility[id];
                }
            }
        }
        return result;
    }

    protected boolean atLeastTwoPlayersNotDisqualified() {
        return getNrOfDisqualifiedAgents() <= (gameAgents.size() - 2);
    }

    protected boolean isEveryoneExceptOneDisqualified() {
        return getNrOfDisqualifiedAgents() == (gameAgents.size() - 1);
    }

    protected boolean isEveryoneDisqualified() {
        return getNrOfDisqualifiedAgents() == gameAgents.size();
    }

    protected int getNrOfDisqualifiedAgents() {
        return (int) gameAgents.values().stream().filter(Agent::isDisqualified).count();
    }

    private String getGameFileUrl() {
        var classLoader = (URLClassLoader) getGame().getClass().getClassLoader();
        return URLDecoder.decode(classLoader.getURLs()[0].toExternalForm(), StandardCharsets.UTF_8);
    }

}
