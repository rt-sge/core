package at.ac.tuwien.ifs.sge.core.engine.factory;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.engine.game.match.TurnBasedMatch;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.TurnBasedGame;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class TurnBasedMatchFactory
        extends MatchFactory {

    private final Logger log;
    private final ExecutorService threadPool;
    private final boolean showTextRepresentation;
    private final long computationTime;
    private final TimeUnit timeUnit;
    private final int maxActions;

    public TurnBasedMatchFactory(GameFactory<?> gameFactory,
                                 GameConfiguration gameConfiguration,
                                 int nrOfPlayers,
                                 Logger log,
                                 ExecutorService threadPool,
                                 boolean showTextRepresentation,
                                 long computationTime,
                                 TimeUnit timeUnit,
                                 int maxActions) {
        super(gameFactory, gameConfiguration, nrOfPlayers);
        this.log = log;
        this.threadPool = threadPool;
        this.showTextRepresentation = showTextRepresentation;
        this.computationTime = computationTime;
        this.timeUnit = timeUnit;
        this.maxActions = maxActions;
    }

    @Override
    public Match<?, ?> newMatch(HashMap<Integer, Agent> agentMap) {
        var game = newGameInstance();
        return new TurnBasedMatch<>((TurnBasedGame<?, ?>) game, agentMap, computationTime, timeUnit, log, threadPool, maxActions, showTextRepresentation);
    }

}
