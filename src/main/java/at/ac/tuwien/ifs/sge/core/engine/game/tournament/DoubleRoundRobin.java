package at.ac.tuwien.ifs.sge.core.engine.game.tournament;

import at.ac.tuwien.ifs.sge.core.engine.factory.MatchFactory;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;

import java.security.cert.CertificateEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class DoubleRoundRobin extends RoundRobin {


  public DoubleRoundRobin(MatchFactory matchFactory,
                          List<Agent> gameAgents,
                          int nrOfPlayersPerMatch,
                          Logger log) {
    super(  matchFactory,
            gameAgents,
            nrOfPlayersPerMatch,
            log);
  }

  @Override
  public List<MatchResult> call() throws ExecutionException, InterruptedException, CertificateEncodingException {
    if (tournamentResults.size() == 0) {
      int gameAgentsSize = gameAgents.size();
      int numberOfGames = Util.nPr(gameAgentsSize, nrOfPlayersPerMatch);

      log.info("Starting Double Round Robin tournament with " + gameAgentsSize + " contestant" + (
              gameAgentsSize == 1 ? "" : "s") + ", " + numberOfGames + " game" + (numberOfGames == 1
              ? "" : "s") + ".");
      log.debug(
              "Agents: " + gameAgents.stream().map(Objects::toString)
                      .collect(Collectors.joining(", ")));

      if (nrOfPlayersPerMatch == 2) {
        oneVsOneTournament();
      } else {
        tournament();
      }
    }
    return Collections.unmodifiableList(tournamentResults);
  }

  @Override
  public TournamentMode getMode() {
    return TournamentMode.DOUBLE_ROUND_ROBIN;
  }

  @Override
  public String toTextRepresentation() {
      if (tournamentResults.size() == 0) {
        return "not played";
      } else if (textRepresentation == null) {
        if (oneVsOne) {
          textRepresentation = createOneVsOneStringTable();
        } else {
          textRepresentation = createResultTable();
        }
      }
      return textRepresentation;
  }

  private void oneVsOneTournament() throws ExecutionException, InterruptedException, CertificateEncodingException {

    for (int x = 0; x < gameAgents.size(); x++) {
      for (int y = 0; y < gameAgents.size(); y++) {
        if (x != y) {
          var xAgent = gameAgents.get(x);
          var yAgent = gameAgents.get(y);

          var match = createOneVsOneMatch(xAgent, yAgent);
          nextMatchSubject.onNext(match);

          var matchResult = match.call();
          onMatchResult(matchResult);
        }
      }
    }


  }

  private void tournament() throws ExecutionException, InterruptedException, CertificateEncodingException {

    final int n = gameAgents.size() - 1;

    var indices = new int[nrOfPlayersPerMatch];
    for (int i = 0; i < nrOfPlayersPerMatch; i++) {
      indices[i] = (nrOfPlayersPerMatch - 1) - i;
    }

    do {

      List<Agent> agentList = Arrays.stream(indices).mapToObj(gameAgents::get)
          .collect(Collectors.toList());
      var playerMap = Util.getPlayerMap(agentList);

      var match = matchFactory.newMatch(playerMap);
      nextMatchSubject.onNext(match);

      var matchResult = match.call();
      onMatchResult(matchResult);

      indices = Util.permutations(indices, n);
    } while (!Util.isReverseIndexEqualToValue(indices));

  }

}
