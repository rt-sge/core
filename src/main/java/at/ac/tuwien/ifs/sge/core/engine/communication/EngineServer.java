package at.ac.tuwien.ifs.sge.core.engine.communication;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.security.PrivateKeyCertificatePair;
import at.ac.tuwien.ifs.sge.core.security.SecurityUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.security.*;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;


public class EngineServer implements Runnable {

    public final static int DEFAULT_PORT = 8000;

    private final Logger log;
    private final ExecutorService pool;
    private final ClassLoader classLoader;
    private final SecureRandom secureRandom = new SecureRandom();
    private final Stack<PrivateKeyCertificatePair> clientPrivateKeyCertificatePairs = new Stack<>();
    private final int nrOfClients;
    private X509Certificate serverCertificate;

    private final ArrayList<AgentConnection> connections = new ArrayList<>();

    private final BlockingQueue<AgentMessage> messageQueue = new ArrayBlockingQueue<>(1024);
    private int port = 0;
    private SSLServerSocket serverSocket;


    public EngineServer(int port, Logger log, ExecutorService pool, ClassLoader classLoader, int nrOfClients) {
        this.log = log;
        this.pool = pool;
        this.classLoader = classLoader;
        if (port == 0) port = DEFAULT_PORT;
        this.port = port;
        this.nrOfClients = nrOfClients;
        try {
            Security.addProvider(new BouncyCastleProvider());
            var sslServerSockterFactory = getServerSocketFactory();
            assert sslServerSockterFactory != null;
            var address = new InetSocketAddress(InetAddress.getLoopbackAddress(), port);
            serverSocket = (SSLServerSocket) sslServerSockterFactory.createServerSocket();
            serverSocket.setNeedClientAuth(true);
            serverSocket.bind(address);
        }
        catch (IOException e) {
            log.error("Exception creating server socket: " + e);
        }
        log.debug("Server started, listening on port " + port);
    }

    public void run() {
        for(;;) {
            try {
            SSLSocket client = (SSLSocket) serverSocket.accept();
            log.trace("New connection accepted from port " + client.getPort());
            AgentConnection connection = new AgentConnection(this, client, log, classLoader);
            pool.submit(connection);
            connections.add(connection);
            } catch (SocketException e) {
                if (!serverSocket.isClosed())
                    log.error("Exception while listening for connections: " + e);
                break;
            } catch (IOException e) {
                log.error("Exception while listening for connections: " + e);
                break;
            }
        }
    }

    public void stop() {
        closeConnections();
        log.deb_("Closing sge server");
        try {
            serverSocket.close();
            log._debug(", done.");
        } catch (IOException e) {
            log.error("Could not close server socket: " + e);
        }
    }

    public void closeConnections() {
        for (var connection : connections) {
            connection.close();
        }
        connections.clear();
    }

    public void declinePlayerClaim(AgentConnection connection) {
        connection.close();
    }

    public AgentMessage readMessage() throws InterruptedException {
        return messageQueue.take();
    }

    public void onClientDataReceived(AgentMessage message) {
        messageQueue.add(message);
    }

    public X509Certificate getServerCertificate() {
        return serverCertificate;
    }

    public PrivateKeyCertificatePair popClientPrivateKeyCertificatePair() {
        return clientPrivateKeyCertificatePairs.empty() ? null : clientPrivateKeyCertificatePairs.pop();
    }

    private SSLServerSocketFactory getServerSocketFactory() {

        SSLServerSocketFactory factory = null;
        try {
            var context = SSLContext.getInstance("TLS");
            var kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            var tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());

            var keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            var trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            var serverKeyPair = SecurityUtil.generateEd25519KeyPair(secureRandom);
            serverCertificate  = SecurityUtil.generateSelfSignedEd25519Certificate("CN=localhost", serverKeyPair);

            for (var i = 0; i < nrOfClients; i++) {
                var clientKeyPair = SecurityUtil.generateEd25519KeyPair(secureRandom);
                var clientCertificate = SecurityUtil.generateSignedEd25519Certificate("CN=agent" + i, clientKeyPair.getPublic(), serverKeyPair.getPrivate());
                clientPrivateKeyCertificatePairs.add(new PrivateKeyCertificatePair(clientKeyPair.getPrivate(), clientCertificate));
                trustStore.setCertificateEntry("client-" + i, clientCertificate);
            }
            tmf.init(trustStore);

            keyStore.load(null, "changeit".toCharArray());
            var chain = new X509Certificate[]{serverCertificate};
            keyStore.setKeyEntry("sge-server", serverKeyPair.getPrivate(), "changeit".toCharArray(), chain);
            kmf.init(keyStore,  "changeit".toCharArray());

            context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            factory = context.getServerSocketFactory();
            return factory;
        } catch (Exception e) {
            log.error("Error starting Server: " + e);
            return null;
        }
    }


}
