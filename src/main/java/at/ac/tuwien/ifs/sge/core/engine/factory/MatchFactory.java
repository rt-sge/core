package at.ac.tuwien.ifs.sge.core.engine.factory;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;

import java.util.HashMap;

abstract public class MatchFactory {

    private final GameFactory<?> gameFactory;
    private final GameConfiguration gameConfiguration;
    private final int nrOfPlayers;

    public MatchFactory(GameFactory<?> gameFactory, GameConfiguration gameConfiguration, int nrOfPlayers) {
        this.gameFactory = gameFactory;
        this.gameConfiguration = gameConfiguration;
        this.nrOfPlayers = nrOfPlayers;
    }

    public abstract Match<?, ?> newMatch(HashMap<Integer, Agent> agentMap);

    public Game<?, ?> newGameInstance() {
        return gameFactory.newInstance(gameConfiguration, nrOfPlayers);
    }

}
