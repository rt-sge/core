package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

abstract public class AbstractRealTimeHumanAgent<G extends RealTimeGame<A, ?>, A> extends AbstractRealTimeGameAgent<G, A> {

    private static final int CMD_WAIT_TIMEOUT_MS = 500;

    public AbstractRealTimeHumanAgent(Class<G> clazz, int playerId, String playerName) {
        super(clazz, playerId, playerName, -2);
    }

    private Future<?> inputFuture;

    @Override
    protected int getMinimumNumberOfThreads() {
        return super.getMinimumNumberOfThreads() + 2;
    }

    @Override
    protected void initializeThreadPool() {
        pool = Executors.newFixedThreadPool(getMinimumNumberOfThreads());
    }

    @Override
    public void setup(GameConfiguration gameConfiguration, int numberOfPlayers) {
        super.setup(gameConfiguration, numberOfPlayers);
    }

    @Override
    public void startPlaying() {

        Scanner scanner = new Scanner(System.in);

        inputFuture = pool.submit(() -> {
            do {
                var possibleActions = new ArrayList<>(getGame().getPossibleActions(playerId));
                if(possibleActions.size() > 0) {
                    log.info("Possible actions: ");
                    for (var i = 0; i < possibleActions.size(); i++) {
                        log.info("[" + i + "] " + possibleActions.get(i).toString());
                    }
                    log.info("Enter the nr of the action to perform: (Or something invalid to refresh!)");
                }
                else{
                    log.info("No actions available");
                }
                var line = scanner.nextLine();
                log.info("Your input: " + line);
                try {
                    var actionNr = Integer.parseInt(line);
                    if (actionNr >= 0 && actionNr < possibleActions.size()) {
                        var action = possibleActions.get(actionNr);
                        sendAction(action, System.currentTimeMillis() + 60);
                        log.info("Sending action: " + action);
                    } else {
                        log.info("Refreshing actions!");
                    }

                } catch (NumberFormatException ignored) {
                }
            } while (true);
        });

    }

    @Override
    public void shutdown() {
        inputFuture.cancel(true);
        log.info("Game Over!");
    }

    @Override
    protected void onGameUpdate(HashMap<A, ActionResult> actionsWithResult) {
    }

    @Override
    protected void onActionRejected(A action)
    {

    }
}
