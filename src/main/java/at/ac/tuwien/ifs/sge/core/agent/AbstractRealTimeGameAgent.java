package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.*;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;

abstract public class AbstractRealTimeGameAgent<G extends RealTimeGame<A, ?>, A>
        extends AbstractGameAgent<G, A>
        implements RealTimeGameAgent<G, A> {

    private static final int MIN_NR_OF_THREADS = 0;

    private G game;
    private G currentGameState;

    public AbstractRealTimeGameAgent(Class<G> gameClass, int playerId, String playerName) {
        super(gameClass, playerId, playerName);
    }

    public AbstractRealTimeGameAgent(Class<G> gameClass, int playerId, String playerName, int logLevel) {
        super(gameClass, playerId, playerName, logLevel);
    }

    @Override
    public void setup(GameConfiguration gameConfiguration, int numberOfPlayers) {
        super.setup(gameConfiguration, numberOfPlayers);
        gameFactory = gameLoader.newGameFactory(gameConfiguration);
        game = gameFactory.newInstance(gameConfiguration, numberOfPlayers);
        currentGameState = (G) game.copy();
    }

    @Override
    protected int getMinimumNumberOfThreads() {
        return super.getMinimumNumberOfThreads() + MIN_NR_OF_THREADS;
    }

    @Override
    protected void initializeThreadPool() {
        pool = Executors.newFixedThreadPool(getMinimumNumberOfThreads());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void start() {
        super.start();
        var gameStarted = false;
        while (!gameStarted) {
            try {
                var event = serverConnection.readMessage();
                if (handleEvent(event)) continue;
                if (event instanceof GameInitializationEvent) {
                    var initEvent = (GameInitializationEvent<A>) event;
                    var actions = initEvent.getInitializationActions();
                    var startTime = initEvent.getStartTimeMs();
                    var gameClock = game.getGameClock();
                    gameClock.start(startTime);
                    for (var action : actions)
                        game.applyAction(action, startTime);
                    currentGameState = (G) game.copy();
                    var initAckEvent = new GameInitializationEvent<>(playerId, null, startTime);
                    serverConnection.sendMessage(initAckEvent);
                }
                if (event instanceof StartGameEvent startGameEvent) {
                    startPlaying();
                    gameStarted = true;
                }
            } catch (InterruptedException e) {
                log.error("interrupted before game could start!");
                log.printStackTrace(e);
                return;
            } catch (ActionException e) {
                log.error("exception while applying intial actions!");
                log.printStackTrace(e);
                return;
            }
        }
        for (; ; ) {
            try {
                var event = serverConnection.readMessage();

                if (handleEvent(event)) continue;

                if (event instanceof InvalidGameActionEvent) {
                    var action = ((InvalidGameActionEvent<A>) event).getActions().stream().findFirst().orElseThrow();
                    log.trace("received invalid game action event: " + action);
                    onActionRejected(action);
                } else if (event instanceof GameUpdateEvent) {
                    var gameUpdateEvent = (GameUpdateEvent<A>) event;
                    var actions = gameUpdateEvent.getActions();
                    var executionTime = gameUpdateEvent.getExecutionTimeMs();
                    log.trace("received game update event with " + actions.size() + " actions:");
                    HashMap<A, ActionResult> actionsWithResult = new HashMap<>();
                    for (var action : actions) {
                        log.trace(action);
                        var result = game.applyAction(action, executionTime);
                        if (!result.wasSuccessful()) {
                            log.warn("Game update from server could not be applied!");
                            log.warn("Agent game and server game are no longer in sync...");
                            log.warn("Did you apply changes to the game from the agent?");
                        }
                        actionsWithResult.put(action, result);
                    }
                    currentGameState = (G) game.copy();
                    onGameUpdate(actionsWithResult);
                }
            } catch (InterruptedException e) {
                break;
            } catch (Exception e) {
                log.printStackTrace(e);
            }
        }
    }

    protected void sendAction(A action, long executionTimeMs) {
        var event = new GameActionEvent<>(playerId, action, executionTimeMs);
        log.trace("sent action: " + action);
        serverConnection.sendMessage(event);
    }

    abstract protected void onGameUpdate(HashMap<A, ActionResult> actionsWithResult);

    abstract protected void onActionRejected(A action);

    /**
     * This method is thread safe.
     *
     * @return A copy of the current game state
     */
    protected synchronized G copyGame() {
        return (G) currentGameState.copy();
    }


    /**
     * Only use this in overridden methods, as they are called from the main thread.
     * Use copyGame() for any other thread.
     *
     * @return The current game state
     */
    protected G getGame() {
        return game;
    }

}
