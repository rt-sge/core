package at.ac.tuwien.ifs.sge.core.agent;

import java.io.File;

public class AgentInfo {

    protected final String name;

    protected final File path;

    protected final boolean realtime;

    public AgentInfo(String name, File path, boolean realtime) {
        this.name = name;
        this.path = path;
        this.realtime = realtime;
    }

    public Agent createAgent(Integer agentId, MemorySpecifications memorySpecifications, boolean createLogFile) {
        return new Agent(agentId, memorySpecifications, createLogFile, name, path, realtime);
    }

    public String getName() {
        return name;
    }

    public File getPath() {
        return path;
    }

    public boolean isRealtime() {
        return realtime;
    }

    @Override
    public String toString() {
        return name;
    }
}
