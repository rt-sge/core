package at.ac.tuwien.ifs.sge.core.game;

public class GameClock {

    private long startTimeMs = 0;
    private long gameTimeMs = 0;

    private float speedFactor = 1;

    public GameClock() {
    }

    public GameClock(GameClock gameClock) {
        startTimeMs = gameClock.getStartTimeMs();
        gameTimeMs = gameClock.getGameTimeMs();
        speedFactor = gameClock.getSpeedFactor();
    }

    public void start() {
        start(System.currentTimeMillis());
    }

    public void start(long startTimeMs) {
        this.startTimeMs = startTimeMs;
        gameTimeMs = startTimeMs;
    }

    public void setStartTimeMs(long startTimeMs) {
        this.startTimeMs = startTimeMs;
    }

    public long getStartTimeMs() {
        return startTimeMs;
    }

    public long getGameDurationMs() {
        return gameTimeMs - startTimeMs;
    }

    public long getGameTimeMs() {
        return gameTimeMs;
    }

    public void setGameTimeMs(long gameTimeMs) {
        this.gameTimeMs = gameTimeMs;
    }

    public float getSpeedFactor() {
        return speedFactor;
    }

    public void setSpeedFactor(float speedFactor) {
        this.speedFactor = speedFactor;
    }
}
