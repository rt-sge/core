package at.ac.tuwien.ifs.sge.core.game;

import java.util.List;

public class InvalidGameActionUpdate<A> extends GameUpdate<A>{
    private final String errorMessage;

    public InvalidGameActionUpdate(A action, Integer player, long executionTimeMs, String errorMessage) {
        super(List.of(action), player, executionTimeMs);
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
