package at.ac.tuwien.ifs.sge.core.engine.loader;

import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.factory.GameFactory;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URLClassLoader;

public class GameLoader<G extends Game<?, ?>> {

  private Class<G> gameClass;

  private String gameClassName;

  private URLClassLoader classLoader;

  private final Logger log;

  public GameLoader(Class<G> gameClass, Logger log) {
    this.gameClass = gameClass;
    this.log = log;
  }

  public GameLoader(String gameClassName, URLClassLoader classLoader, Logger log) {
    this.classLoader = classLoader;
    this.gameClassName = gameClassName;
    this.log = log;
  }

  @SuppressWarnings("unchecked")
  public GameFactory<G> newGameFactory(GameConfiguration gameConfiguration) {
    log.tra_("Loading game file...");
    try {
      if (gameClass == null) {
        gameClass = (Class<G>) classLoader
                .loadClass(gameClassName);
      }
      Constructor<G> gameConstructor = gameClass
              .getConstructor(GameConfiguration.class, int.class, Logger.class);
      Constructor<G> gameConstructorWithoutPlayerNumber = gameClass
              .getConstructor(GameConfiguration.class, Logger.class);
      var testGame = gameConstructorWithoutPlayerNumber.newInstance(gameConfiguration, log);

      log._trace(", done.");
      return new GameFactory<>(gameConstructor,
              testGame,
              log);
    } catch (ClassNotFoundException e) {
      log._trace(", failed.");
      log.error("Could not find class of game.");
      log.printStackTrace(e);
      throw new IllegalStateException("Could not find class of game.");
    } catch (NoSuchMethodException e) {
      log._trace(", failed.");
      log.error("Could not find required constructors of game.");
      log.printStackTrace(e);
      throw new IllegalStateException("Could not find required constructors of game.");
    } catch (IllegalAccessException e) {
      log._trace(", failed.");
      log.error("Not allowed to access constructors of game.");
      log.printStackTrace(e);
      throw new IllegalStateException("Not allowed to access constructors of game.");
    } catch (InvocationTargetException e) {
      log._trace(", failed.");
      log.error("Error while invoking constructors of game.");
      log.printStackTrace(e);
      throw new IllegalStateException("Error while invoking constructors of game.");
    } catch (InstantiationException e) {
      log._trace(", failed.");
      log.error("Error while instantiating game.");
      log.printStackTrace(e);
      throw new IllegalStateException("Error while instantiating game.");
    } catch (AbstractMethodError e) {
      log._trace(", failed.");
      log.error("Error while calling method declared by interface.");
      throw new IllegalStateException("Error while calling method declared by interface.");
    }
  }

  public Class<G> getGameClass() {
    return gameClass;
  }

  public ClassLoader getClassLoader() {
    ClassLoader loader = null;
    if (gameClass != null)
      loader = gameClass.getClassLoader();
    return loader;
  }

  public URLClassLoader getURLClassLoader() {
    return classLoader;
  }

}
