package at.ac.tuwien.ifs.sge.core.game;

import java.util.List;
import java.util.Objects;

public class TimedActionRecord<A> extends ActionRecord<A> {

    private final long timestampMs;

    public TimedActionRecord(TimedActionRecord<A> a) {
        this(a.timestampMs, a.action);
    }

    public TimedActionRecord(long timestampMs, A action) {
        super(-1, action);
        this.timestampMs = timestampMs;
    }

    public long getTimestampMs() {
        return timestampMs;
    }

    @Override
    public String toString() {
        return "<" + timestampMs + ", " + player + ", " + action + '>';
    }

    @Override
    public boolean equals(Object o) {
        TimedActionRecord<?> that = (TimedActionRecord<?>) o;
        return super.equals(o)
                && timestampMs == that.timestampMs;
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestampMs, player, action);
    }


    public static <A> String iterableToTimedString(List<TimedActionRecord<A>> actionRecords) {
        if (actionRecords.isEmpty()) {
            return "<>";
        }

        int lastPlayer = actionRecords.get(0).getPlayer() + 1;

        var builder = new StringBuilder();

        boolean flushPlayer = false;

        for (var actionRecord : actionRecords) {
            if (actionRecord.getPlayer() != lastPlayer) {
                if (flushPlayer) {
                    builder.append("> \n");
                }
                builder.append('<');
                if (actionRecord.getPlayer() >= 0) {
                    builder.append(actionRecord.getPlayer());
                } else {
                    builder.append('-');
                }
                builder.append(", ");
                flushPlayer = true;

            }

            builder.append("[")
                    .append(actionRecord.getTimestampMs())
                    .append(", ")
                    .append(actionRecord.getAction().toString())
                    .append(']');
            lastPlayer = actionRecord.getPlayer();

        }

        builder.append('>');

        return builder.toString();
    }
}
