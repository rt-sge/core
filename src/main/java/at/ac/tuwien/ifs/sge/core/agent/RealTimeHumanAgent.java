package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;

public class RealTimeHumanAgent<G extends RealTimeGame<A, ?>, A> extends AbstractRealTimeHumanAgent<G, A> {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        var playerId = getPlayerIdFromArgs(args);
        var playerName = getPlayerNameFromArgs(args);
        var gameClass = ( Class<? extends RealTimeGame<Object, Object>> ) getGameClassFromArgs(args);
        var agent = new RealTimeHumanAgent<>(gameClass, playerId, playerName);
        agent.start();
    }

    public RealTimeHumanAgent(Class<G> clazz, int playerId, String playerName) {
        super(clazz, playerId, playerName);
    }
}
