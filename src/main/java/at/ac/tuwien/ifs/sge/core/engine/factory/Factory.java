package at.ac.tuwien.ifs.sge.core.engine.factory;

public interface Factory<E> {

  E newInstance(Object... initargs);

}
