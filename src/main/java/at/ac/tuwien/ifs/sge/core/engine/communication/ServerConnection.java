package at.ac.tuwien.ifs.sge.core.engine.communication;

import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.SgeEvent;
import at.ac.tuwien.ifs.sge.core.security.SecurityUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.net.ssl.*;
import java.io.*;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.*;

public class ServerConnection {

    public static int NR_OF_TASKS = 2;

    private SSLSocket socket;
    private final Logger log;

    private ObjectInputStream in;
    private ObjectOutputStream out;
    private CountDownLatch outputLatch;
    private final SecureRandom secureRandom = new SecureRandom();

    private final BlockingQueue<SgeEvent> inputQueue = new ArrayBlockingQueue<>(1024);
    private final BlockingQueue<SgeEvent> outputQueue = new ArrayBlockingQueue<>(1024);

    public ServerConnection(Logger log, ClassLoader classLoader) {
        this.log = log;
        Security.addProvider(new BouncyCastleProvider());
        SSLSocketFactory factory = getSocketFactory();
        if (factory == null) return;
        try {
            socket = (SSLSocket) factory.createSocket(InetAddress.getLoopbackAddress(), EngineServer.DEFAULT_PORT);
            log.debug("Connected to " + socket.getInetAddress() + ":" + socket.getPort());
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream()) {
                @Override
                public Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
                    try {
                        return super.resolveClass(desc);
                    } catch (ClassNotFoundException e) {
                        return Class.forName(desc.getName(), true, classLoader);
                    }
                }
            };
        } catch (IOException e) {
            log.error("Error while connecting to sge server: " + e);
        }
    }

    public List<Runnable> getTasks() {
        Runnable inputThread = () -> {
            for (;;) {
                try {
                    var input = in.readObject();
                    if (!(input instanceof SgeEvent message)) continue;
                    inputQueue.add(message);
                } catch (IOException e) {
                    if (e instanceof EOFException) {
                        close();
                    } else if (!socket.isClosed()) {
                        log.error("Error while reading message from server: ");
                        log.printStackTrace(e);
                    }
                    break;
                } catch (ClassNotFoundException e) {
                    log.error("Class for received object was not found: " + e);
                    break;
                }
            }
        };

        Runnable outputThread = () -> {
            for (;;) {
                try {
                    var output = outputQueue.take();
                    out.writeObject(output);
                    if (outputLatch != null && outputLatch.getCount() != 0)
                        outputLatch.countDown();
                } catch (IOException e) {
                    log.error("Error while sending object to sge server: " + e);
                } catch (InterruptedException e) {
                    break;
                }
            }
        };
        return List.of(inputThread, outputThread);
    }

    public SgeEvent readMessage() throws InterruptedException {
        return inputQueue.take();
    }

    public void sendMessage(SgeEvent event) {
        outputQueue.add(event);
    }

    public boolean sendMessageAndWait(SgeEvent event, long timeoutMs) throws InterruptedException {
        outputLatch = new CountDownLatch(outputQueue.size() + 1);
        sendMessage(event);
        return outputLatch.await(timeoutMs, TimeUnit.MILLISECONDS);
    }

    public void close() {
        if (socket.isClosed()) return;
        try {
            socket.close();
            log.debug("Closing connection with sge server...");
        } catch (IOException e) {
            log.error("Could not close client connection: " + e);
        }
    }
    private static SSLSocketFactory getSocketFactory() {
        try {
            var context = SSLContext.getInstance("TLS");
            var tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            var kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            var trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            var keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

            var certificateFactory = CertificateFactory.getInstance("X.509");
            var serverCertPem = System.getenv(Agent.SERVER_CERTIFICATE_KEY);
            var serverCertInputStream =  new ByteArrayInputStream(serverCertPem.getBytes(StandardCharsets.UTF_8));
            var certificate = certificateFactory.generateCertificate(serverCertInputStream);
            trustStore.load(null, null);
            trustStore.setCertificateEntry("sge-server", certificate);
            tmf.init(trustStore);

            var agentPrivateKey = SecurityUtil.base64StringToEd25519PrivateKey(System.getenv(Agent.AGENT_PRIVATE_KEY));

            var agentCertPem = System.getenv(Agent.AGENT_CERTIFICATE_KEY);
            var agentCertInputStream =  new ByteArrayInputStream(agentCertPem.getBytes(StandardCharsets.UTF_8));
            var agentCertificate = (X509Certificate) certificateFactory.generateCertificate(agentCertInputStream);

            keyStore.load(null, "changeit".toCharArray());
            var chain = new X509Certificate[]{agentCertificate};
            keyStore.setKeyEntry("agent", agentPrivateKey, "changeit".toCharArray(), chain);
            kmf.init(keyStore,  "changeit".toCharArray());

            context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            return context.getSocketFactory();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
