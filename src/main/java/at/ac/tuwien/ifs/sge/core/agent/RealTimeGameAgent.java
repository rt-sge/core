package at.ac.tuwien.ifs.sge.core.agent;


import at.ac.tuwien.ifs.sge.core.game.Game;

public interface RealTimeGameAgent<G extends Game<A, ?>, A> extends GameAgent<G, A> {

    /**
     * Called when the game starts and the agent is allowed to send actions.
     * Actions that are sent before this method is called have no impact on the game.
     */
    default void startPlaying() {

    }
}
