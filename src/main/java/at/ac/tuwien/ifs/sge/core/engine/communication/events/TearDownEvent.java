package at.ac.tuwien.ifs.sge.core.engine.communication.events;

public class TearDownEvent extends SgeEvent {
    public TearDownEvent() {
    }

    public TearDownEvent(Integer playerId) {
        super(playerId);
    }
}
