package at.ac.tuwien.ifs.sge.core.engine.game.match;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.communication.*;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.ComputeNextActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.SgeEvent;
import at.ac.tuwien.ifs.sge.core.game.TurnBasedGame;
import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;
import at.ac.tuwien.ifs.sge.core.util.Util;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.security.cert.CertificateEncodingException;
import java.util.*;
import java.util.concurrent.*;

public class TurnBasedMatch<G extends TurnBasedGame<A, ?>, A> extends
        Match<G, A> {

    private final PublishSubject<A> visualizationUpdateSubject = PublishSubject.create();
    private Future<GameActionEvent<A>> agentActionsThreadFuture;

    private G game;
    protected final int maxActions;
    protected final long computationTime;
    protected final TimeUnit timeUnit;

    public TurnBasedMatch(G game,
                          HashMap<Integer, Agent> gameAgents,
                          long computationTime,
                          TimeUnit timeUnit,
                          Logger log,
                          ExecutorService pool,
                          int maxActions,
                          boolean showTextRepresentation) {
        super(gameAgents, log, pool, showTextRepresentation);
        this.game = game;
        this.computationTime = computationTime;
        this.timeUnit = timeUnit;
        this.maxActions = maxActions;
        if (game.getNumberOfPlayers() != gameAgents.size()) {
            throw new IllegalArgumentException("Not the correct number of players");
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public MatchResult call() throws InterruptedException, ExecutionException, CertificateEncodingException {

        if (matchResult != null) {
            return matchResult;
        }

        long startTime = System.nanoTime();

        try {
            startServer();

            startAgents();

            setUpAgents();

            printConfiguration();

            int lastPlayer = (-1);
            int currentPlayer;
            var nrOfActions = 0;

            // track disqualification success since some games dont support disqualification of players
            var successfulDisqualification = true;
            for (var entrySet : gameAgents.entrySet()) {
                var id = entrySet.getKey();
                var agent = entrySet.getValue();
                if (agent.isDisqualified()) {
                    successfulDisqualification = successfulDisqualification && game.disqualifyPlayer(id);
                }
            }


            while (!game.isGameOver()
                    && successfulDisqualification
                    && atLeastTwoPlayersNotDisqualified()
                    && nrOfActions < maxActions) {

                currentPlayer = game.getCurrentPlayer();

                if (currentPlayer >= 0) {

                    G playersGame = (G) game.getGame(currentPlayer);

                    log.inf_(getPlayerPrefix(currentPlayer));


                    var agent = gameAgents.get(currentPlayer);
                    var connection = agent.getConnection();
                    var computeNextActionEvent = new ComputeNextActionEvent<>(currentPlayer, playersGame, computationTime, timeUnit);

                    connection.sendMessage(computeNextActionEvent);


                    final int finalCurrentPlayer = currentPlayer;
                    final Callable<GameActionEvent<A>> getPlayerResponse = () -> {
                        AgentMessage response;
                        SgeEvent content;
                        do {
                            response = server.readMessage();
                            content = response.getContent();
                        } while (response.getPlayerId() != finalCurrentPlayer
                                && !(content instanceof GameActionEvent));
                        return (GameActionEvent<A>) content;
                    };

                    agentActionsThreadFuture = pool.submit(getPlayerResponse);


                    GameActionEvent<A> actionEvent;
                    try {
                        timeLimitMsSubject.onNext(timeUnit.toMillis(computationTime));
                        actionEvent = agentActionsThreadFuture.get(computationTime, timeUnit);
                    } catch (TimeoutException e) {
                        log.warn("Agent timed out.");
                        gameAgents.get(currentPlayer).setDisqualified(true);
                        successfulDisqualification = game.disqualifyPlayer(currentPlayer);
                        continue;
                    }
                    A action = actionEvent == null ? null : actionEvent.getAction();


                    if (action == null) {
                        log.warn("No action given.");
                        gameAgents.get(currentPlayer).setDisqualified(true);
                        successfulDisqualification = game.disqualifyPlayer(currentPlayer);
                        continue;
                    }


                    if (!game.isValidAction(action)) {
                        log.warn("Illegal action given.");
                        try {
                            game.doAction(action);
                        } catch (IllegalArgumentException e) {
                            log.printStackTrace(e);
                        }
                        gameAgents.get(currentPlayer).setDisqualified(true);
                        successfulDisqualification = game.disqualifyPlayer(currentPlayer);
                        continue;
                    }

                    log._info(action);
                    game = (G) game.doAction(action);
                    visualizationUpdateSubject.onNext(action);
                    lastPlayer = currentPlayer;

                } else {

                    A action = game.determineNextAction();
                    if (action == null || !game.isValidAction(action)) {
                        log.err_(
                                "There is a programming error in the implementation of the game.");
                        if (action == null) {
                            log._error(" Next action is null.");
                        } else {
                            log._error(" Next action is invalid.");
                        }
                        throw new IllegalStateException("The current game violates the implementation contract");
                    }

                    log.info("Game action: " + action);
                    game = (G) game.doAction(action);
                    visualizationUpdateSubject.onNext(action);
                }
                nrOfActions++;

                if (game.getCurrentPlayer() >= 0 && lastPlayer != game
                        .getCurrentPlayer() && showTextRepresentation) {
                    printTextualRepresentation();
                }

            }
            var endtime = System.nanoTime();

            tearDownAgents();

            printGameOver(true);

            matchResult = new MatchResult(gameAgents, startTime, endtime, calculateGameResult());
            return matchResult;
        } catch (IllegalStateException | InterruptedException | CertificateEncodingException | ExecutionException e) {
            var endtime = System.nanoTime();
            log.info("Game interrupted!");
            log.debugPrintStackTrace(e);

            if (agentActionsThreadFuture != null)
                agentActionsThreadFuture.cancel(true);
            if (playersClaimed)
                tearDownAgents();
            stopMatch(startTime, endtime, true);

            throw e;
        } finally {
            server.stop();
            terminateAgentProcesses();
            removeAgentShutdownHook();
            resetAgents();
        }
    }

    @Override
    public G getGame() {
        return game;
    }

    public PublishSubject<A> getVisualizationUpdateSubject() {
        return visualizationUpdateSubject;
    }

    private void printConfiguration() {
        log.tracef("Computation time: %s", Util.convertUnitToMinimalString(computationTime, timeUnit));
        if (maxActions < Integer.MAX_VALUE - 1) {
            log.tracef("Maximum number of actions: %s", maxActions);
            log.tracef("Max completion time: %s",
                    Util.convertUnitToMinimalString(computationTime * maxActions, timeUnit));
        }
    }

    private void printTextualRepresentation() {
        String textualRepresentation = game.getGame().toTextRepresentation();
        log._info_(textualRepresentation);
    }

    private String getPlayerPrefix(int playerId) {
        return "Player "
                + playerId
                + " - "
                + gameAgents.get(playerId).toString()
                + ": ";
    }

}
