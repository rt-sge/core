package at.ac.tuwien.ifs.sge.core.engine.communication.events;

import java.util.List;

public class GameUpdateEvent<A> extends SgeEvent {

    private final List<A> actions;
    private final long executionTimeMs;

    public GameUpdateEvent(Integer playerId, List<A> actions, long executionTimeMs) {
        super(playerId);
        this.actions = actions;
        this.executionTimeMs = executionTimeMs;
    }

    public List<A> getActions() {
        return actions;
    }

    public long getExecutionTimeMs() {
        return executionTimeMs;
    }
}
