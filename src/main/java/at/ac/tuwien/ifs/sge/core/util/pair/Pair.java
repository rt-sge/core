package at.ac.tuwien.ifs.sge.core.util.pair;

public interface Pair<A, B> {

  A getA();

  B getB();

}
