package at.ac.tuwien.ifs.sge.core.engine.communication;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.SgeEvent;

public class AgentMessage {

    private final AgentConnection connection;

    private final SgeEvent content;

    public AgentMessage(AgentConnection connection, SgeEvent content) {
        this.connection = connection;
        this.content = content;
    }

    public AgentConnection getConnection() {
        return connection;
    }

    public String getName() {
        return connection.getAgentName();
    }

    public Integer getPlayerId() {
        return connection.getAgentPlayerId();
    }

    public SgeEvent getContent() {
        return content;
    }
}
