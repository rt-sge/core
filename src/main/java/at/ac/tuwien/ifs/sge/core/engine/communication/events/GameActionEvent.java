package at.ac.tuwien.ifs.sge.core.engine.communication.events;

public class GameActionEvent<A> extends SgeEvent {

    public static final int INTERNAL_EVENT_ID = -1;

    public static <A> GameActionEvent<A> internal(A action) {
        return internal(action, System.currentTimeMillis() + 1);
    }

    public static <A> GameActionEvent<A> internal(A action, long executionTimeMs) {
        return new GameActionEvent<>(INTERNAL_EVENT_ID, action, executionTimeMs);
    }

    private final A action;

    private final long executionTimeMs;

    public GameActionEvent(Integer playerId, A action, long executionTimeMs) {
        super(playerId);
        this.action = action;
        this.executionTimeMs = executionTimeMs;
    }

    public A getAction() {
        return action;
    }

    public long getExecutionTimeMs() {
        return executionTimeMs;
    }
}
