package at.ac.tuwien.ifs.sge.core.engine.game.tournament;

import at.ac.tuwien.ifs.sge.core.engine.factory.MatchFactory;
import at.ac.tuwien.ifs.sge.core.agent.Agent;
import at.ac.tuwien.ifs.sge.core.engine.game.match.Match;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.game.MatchResult;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.core.util.pair.MutablePair;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.util.*;

public abstract class AbstractTournament implements
    Tournament {


  protected final PublishSubject<Match<?, ?>> nextMatchSubject = PublishSubject.create();
  protected final PublishSubject<MatchResult> nextMatchResultSubject = PublishSubject.create();

  protected final List<MatchResult> tournamentResults = new ArrayList<>();
  protected final Map<Agent, MutablePair<Double, Double>> result = new HashMap<>();

  protected final MatchFactory matchFactory;
  protected final List<Agent> gameAgents;
  protected final int nrOfPlayersPerMatch;
  protected final Logger log;

  protected String textRepresentation;


  protected AbstractTournament(MatchFactory matchFactory,
                               List<Agent> gameAgents,
                               int nrOfPlayersPerMatch,
                               Logger log) {
    this.matchFactory = matchFactory;
    this.gameAgents = gameAgents;
    this.nrOfPlayersPerMatch = nrOfPlayersPerMatch;
    this.log = log;

    this.textRepresentation = null;
  }

  @Override
  public String toTextRepresentation() {
    return textRepresentation;
  }

  @Override
  public PublishSubject<Match<?, ?>> getNextMatchSubject() {
    return nextMatchSubject;
  }

  @Override
  public PublishSubject<MatchResult> getNextMatchResultSubject() {
    return nextMatchResultSubject;
  }


  @Override
  public List<Agent> getAgents() {
    return gameAgents;
  }

  @Override
  public int getNrOfPlayersPerMatch() {
    return nrOfPlayersPerMatch;
  }

  public Map<Agent, MutablePair<Double, Double>> getResult() {
    return result;
  }

  protected String createResultTable() {
    StringBuilder stringBuilder = new StringBuilder();

    List<String> agentNames = gameAgents.stream()
            .map(Agent::toString).toList();
    List<Integer> agentNamesWidth = agentNames.stream()
            .map(String::length).toList();

    String scoreString = "Score";
    String utilityString = "Utility";
    final int firstColumnWidth = Math.max(scoreString.length(), utilityString.length());

    MatchResult.appendSeparationLine(stringBuilder, firstColumnWidth, agentNamesWidth);

    stringBuilder.append('|');
    Util.appendNTimes(stringBuilder, ' ', firstColumnWidth + 2).append('|').append(' ')
            .append(String.join(" | ", agentNames)).append(' ')
            .append('|').append('\n');

    MatchResult.appendSeparationLine(stringBuilder, firstColumnWidth, agentNamesWidth);

    stringBuilder.append('|');
    Util.appendWithBlankBuffer(stringBuilder, scoreString, firstColumnWidth + 2);
    for (var agent : gameAgents) {
      var agentName = agent.toString();
      stringBuilder.append('|');
      if (result.containsKey(agent)) {
        var scoreValue = result.get(agent).getA();
        stringBuilder.append(' ');
        Util.appendWithBlankBuffer(stringBuilder, scoreValue, agentName.length()).append(' ');
      } else {
        Util.appendNTimes(stringBuilder, ' ', agentName.length() + 2);
      }
    }
    stringBuilder.append('|').append('\n');

    stringBuilder.append('|');
    Util.appendWithBlankBuffer(stringBuilder, utilityString, firstColumnWidth + 2);
    for (var agent : gameAgents) {
      var agentName = agent.toString();
      stringBuilder.append('|');
      if (result.containsKey(agent)) {
        double utilityValue = result.get(agent).getB();
        stringBuilder.append(' ');
        Util.appendWithBlankBuffer(stringBuilder, utilityValue, agentName.length()).append(' ');
      } else {
        Util.appendNTimes(stringBuilder, ' ', agentName.length() + 2);
      }
    }
    stringBuilder.append('|').append('\n');

    MatchResult.appendSeparationLine(stringBuilder, firstColumnWidth, agentNamesWidth);

    return stringBuilder.toString();
  }

}
