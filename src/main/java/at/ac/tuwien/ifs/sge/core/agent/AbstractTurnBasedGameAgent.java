package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.ComputeNextActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.TurnBasedGame;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public abstract class AbstractTurnBasedGameAgent<G extends TurnBasedGame<A, ?>, A>
        extends AbstractGameAgent<G, A>
        implements TurnBasedGameAgent<G, A> {

    private static final int MIN_NR_OF_THREADS = 1;
    private static final long ONE_SECOND = TimeUnit.SECONDS.toNanos(1L);

    protected G game;
    protected long computationStartTime;
    protected long computationActuralTimeout;
    protected long computationTimeout;

    private long timeoutMultiplier;
    private long timeoutDivisor;
    private long computationAtLeastTime;

    private Future<?> computeNextActionThreadFuture;

    private ComputeNextActionEvent<G> nextComputeNextActionEvent;
    private final Object computeNextActionLock = new Object();

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId) {
        this(gameClass, playerId, null,0);
    }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, String playerName) { this(gameClass, playerId, playerName, 0); }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, String playerName, int logLevel) {
        this(gameClass, playerId, playerName, 1L, 2L, logLevel);
    }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, String playerName, long timeOutMultiplier, long timeOutDivisor, int logLevel) {
        this(gameClass, playerId, playerName, timeOutMultiplier, timeOutDivisor, 10L, TimeUnit.SECONDS, logLevel);
    }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, String playerName, long timeOutMultiplier, long timeOutDivisor, long atLeast,
                                TimeUnit atLeastTimeUnit, int logLevel) {
        super(gameClass, playerId, playerName, logLevel);
        initializeTimeout(timeOutMultiplier, timeOutDivisor, atLeast, atLeastTimeUnit);

    }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, double timeOutRatio, int logLevel) {
        this(gameClass, playerId, null, timeOutRatio, logLevel);
    }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, String playerName, double timeOutRatio, int logLevel) {
        this(gameClass, playerId, playerName, timeOutRatio, 10L, TimeUnit.SECONDS, logLevel);
    }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, String playerName, double timeOutRatio, long precision, int logLevel) {
        this(gameClass, playerId, playerName, timeOutRatio, precision, 10L, TimeUnit.SECONDS, logLevel);
    }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, String playerName, double timeOutRatio, long atLeast, TimeUnit atLeastTimeUnit,
                                int logLevel) {
        this(gameClass, playerId, playerName, timeOutRatio, TimeUnit.MILLISECONDS.toNanos(1L), atLeast, atLeastTimeUnit, logLevel);
    }

    protected AbstractTurnBasedGameAgent(Class<G> gameClass, int playerId, String playerName, double timeOutRatio, long precision, long atLeast,
                                TimeUnit atLeastTimeUnit, int logLevel) {
        super(gameClass, playerId, playerName, logLevel);
        long timeOutDivisor = 1;
        long timeOutMultiplier = (long) (timeOutRatio / timeOutDivisor);

        while ((double) timeOutMultiplier / (double) timeOutDivisor != timeOutRatio
                || timeOutDivisor >= precision) {
            timeOutDivisor *= 10;
            timeOutMultiplier = (long) (timeOutDivisor * timeOutRatio);
        }
        initializeTimeout(timeOutMultiplier, timeOutDivisor, atLeast, atLeastTimeUnit);
    }

    private void initializeTimeout(
                                   long timeOutMultiplier,
                                   long timeOutDivisor,
                                   long atLeast,
                                   TimeUnit atLeastTimeUnit) {
        timeoutMultiplier = timeOutMultiplier;
        timeoutDivisor = timeOutDivisor;
        computationAtLeastTime = atLeastTimeUnit.toNanos(atLeast);
    }

    @Override
    public void setup(GameConfiguration gameConfiguration, int numberOfPlayers) {
        super.setup(gameConfiguration, numberOfPlayers);
    }


    @SuppressWarnings("unchecked")
    @Override
    public void start() {
        super.start();
        computeNextActionThreadFuture = pool.submit(() -> {
            for (;;) {
                synchronized (computeNextActionLock) {
                    while (nextComputeNextActionEvent == null) {
                        try {
                            computeNextActionLock.wait();
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                    if (nextComputeNextActionEvent != null) {
                        game = nextComputeNextActionEvent.getGame();
                        var computationTime = nextComputeNextActionEvent.getComputationTime();
                        var timeUnit = nextComputeNextActionEvent.getTimeUnit();
                        var action = computeNextAction(game, computationTime, timeUnit);
                        log.trace("computed next action: " + action);
                        var gameActionEvent = new GameActionEvent<>(playerId, action, System.currentTimeMillis());
                        serverConnection.sendMessage(gameActionEvent);
                        nextComputeNextActionEvent = null;
                    } else break;
                }
            }
        });
        for (;;) {
            try {
                var event = serverConnection.readMessage();
                if (handleEvent(event)) continue;
                if (event instanceof ComputeNextActionEvent) {
                    synchronized (computeNextActionLock) {
                        nextComputeNextActionEvent = (ComputeNextActionEvent<G>) event;
                        computeNextActionLock.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                break;
            }
        }
    }

    @Override
    public void shutdown() {
        computeNextActionThreadFuture.cancel(true);
    }

    @Override
    protected int getMinimumNumberOfThreads() {
        return super.getMinimumNumberOfThreads() + MIN_NR_OF_THREADS;
    }

    @Override
    protected void initializeThreadPool() {
        pool = Executors.newFixedThreadPool(getMinimumNumberOfThreads());
    }


    protected void setTimers(long computationTime, TimeUnit timeUnit) {
        computationStartTime = System.nanoTime();
        computationActuralTimeout = timeUnit.toNanos(computationTime);
        long computationTimeInNanos = computationActuralTimeout - ONE_SECOND;
        if (computationTimeInNanos < 0) {
            computationTimeInNanos = computationActuralTimeout;
        }
        computationTimeout = (computationTimeInNanos * timeoutMultiplier) / timeoutDivisor;
        computationTimeout = Math.max(computationTimeout, Math.min(computationTimeInNanos, computationAtLeastTime));
    }

    protected boolean shouldStopComputation(int proportion) {
        return System.nanoTime() - computationStartTime >= (computationTimeout / proportion)
                || !Thread.currentThread().isAlive()
                || Thread.currentThread().isInterrupted();
    }

    protected boolean shouldStopComputation() {
        return shouldStopComputation(1);
    }

    protected long nanosElapsed() {
        return System.nanoTime() - computationStartTime;
    }

    protected long nanosLeft() {
        return computationTimeout - nanosElapsed();
    }
}
