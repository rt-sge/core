package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.security.PrivateKeyCertificatePair;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HumanAgent extends Agent {

    private static final String HUMAN_AGENT_NAME = "Human";

    public static Agent create(Integer agentId, MemorySpecifications memorySpecifications, boolean realtime) {
        return new HumanAgent(agentId, memorySpecifications, HUMAN_AGENT_NAME, realtime);
    }

    public static Agent create(Integer agentId, MemorySpecifications memorySpecifications, boolean realtime, String className,  URLClassLoader classLoader) {
        return new HumanAgent(agentId, memorySpecifications, HUMAN_AGENT_NAME, realtime, className, classLoader);
    }

    private final String className;
    private final String classPath;


    public HumanAgent(Integer agentId,
                      MemorySpecifications memorySpecifications,
                      String name,
                      boolean realtime,
                      String className,
                      URLClassLoader classLoader) {
        super(agentId,
                memorySpecifications,
                false,
                name,
                null,
                realtime);
        this.className = className;
        classPath = Arrays.stream(classLoader.getURLs())
                .map(URL::getFile)
                .collect(Collectors.joining(File.pathSeparator));
    }

    public HumanAgent(Integer agentId,
                      MemorySpecifications memorySpecifications,
                      String name,
                      boolean realtime) {
        super(agentId,
                memorySpecifications,
                false,
                name,
                null,
                realtime);
        this.classPath = URLDecoder.decode(HumanAgent.class.getProtectionDomain().getCodeSource().getLocation().getFile(), StandardCharsets.UTF_8)
                + ";" + URLDecoder.decode(BouncyCastleProvider.class.getProtectionDomain().getCodeSource().getLocation().getFile(), StandardCharsets.UTF_8)
                + ";" + URLDecoder.decode(JcaX509v3CertificateBuilder.class.getProtectionDomain().getCodeSource().getLocation().getFile(), StandardCharsets.UTF_8);
        if (realtime) {
            this.className = RealTimeHumanAgent.class.getName();
        } else {
            this.className = TurnBasedHumanAgent.class.getName();
        }
    }

    @Override
    public Process start(Integer playerId,
                         String gameFilePath,
                         String gameClassName,
                         X509Certificate serverCertificate,
                         PrivateKeyCertificatePair agentPrivateKeyCertificatePair) throws IOException, CertificateEncodingException {
        return start(List.of(CLASSPATH_COMMAND, classPath, className), playerId, gameFilePath, gameClassName, serverCertificate, agentPrivateKeyCertificatePair);
    }
}
