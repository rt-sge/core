package at.ac.tuwien.ifs.sge.core.engine.communication;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.PlayerClaimEvent;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.SgeEvent;

import javax.net.ssl.SSLSocket;
import java.io.*;

public class AgentConnection implements Runnable {

    private static final int MAX_MESSAGE_INTERVAL_VIOLATIONS = 5;
    private static final int MAX_MESSAGE_INTERVAL_NANOS = 10000000; // 10ms

    // private long maxMessageViolation = 0;
    // private long lastMessageNanos;

    private final EngineServer server;
    private final SSLSocket client;
    private final Logger log;

    private String agentName;
    private Integer agentPlayerId;

    private ObjectInputStream in;
    private ObjectOutputStream out;

    public AgentConnection(EngineServer server, SSLSocket client, Logger log, ClassLoader classLoader) {
        this.server = server;
        this.client = client;
        this.log = log;
        // lastMessageNanos = System.nanoTime();
        try {
            in = new ObjectInputStream(client.getInputStream()) {
                @Override
                public Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
                    try {
                        return super.resolveClass(desc);
                    } catch (ClassNotFoundException e) {
                        return Class.forName(desc.getName(), true, classLoader);
                    }
                }
            };
            out = new ObjectOutputStream(client.getOutputStream());
        } catch (IOException e) {
            try {
                client.close();
            } catch (IOException e2) {
                log.error("Could not close client connection: " + e2);
            }
            log.error("Error while getting socket streams: " + e);
        }
    }

    public void run() {
        for (;;) {
            try {
                var input = in.readObject();

                /*var now = System.nanoTime();
                var timeSinceLastMessage = now - lastMessageNanos;
                if (timeSinceLastMessage < MAX_MESSAGE_INTERVAL_NANOS) {
                    maxMessageViolation++;
                    if (maxMessageViolation >= MAX_MESSAGE_INTERVAL_VIOLATIONS) {
                        log.debug(this + " gets disqualified for violating the maximum message interval!");
                        close();
                        return;
                    }
                }
                lastMessageNanos = now;

                 */

                // Only SgeEvents are accepted
                if (!(input instanceof SgeEvent event)) continue;

                if (event instanceof PlayerClaimEvent claim) {
                    agentName = claim.getPlayerName();
                    agentPlayerId = claim.getPlayerId();
                }

                // Events that are from the wrong player are discarded
                if (event.getPlayerId() != agentPlayerId) continue;

                var message = new AgentMessage(this, event);
                server.onClientDataReceived(message);
            } catch (IOException e) {
                if (e instanceof EOFException && !client.isClosed())
                    close();
                else if (!client.isClosed())
                    log.error("Error while reading message from client: " + e);
                break;
            } catch (ClassNotFoundException e) {
                log.error("Received class was not found: " + e);
                break;
            }
        }

    }

    public void sendMessage(SgeEvent event) {
        try {
            out.writeObject(event);
        } catch (IOException e) {
            log.error("Error while sending object to agent: " + this + " -> " + e);
        }
    }


    public void close() {
        if (client.isClosed()) return;
        try {
            client.close();
            log.trace("Closing connection with: " + this);
        } catch (IOException e) {
            log.error("Could not close client connection: " + e);
        }
    }

    public Integer getAgentPlayerId() {
        return agentPlayerId;
    }

    public String getAgentName() {
        return agentName;
    }


    public boolean isClosed() {
        return client.isClosed();
    }

    @Override
    public String toString() {
        return "'" + agentName + "'" + " #" + agentPlayerId + " Port: " + client.getPort();
    }
}
