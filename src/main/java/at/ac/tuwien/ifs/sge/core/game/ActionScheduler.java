package at.ac.tuwien.ifs.sge.core.game;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;

public class ActionScheduler<G extends RealTimeGame<A, ?>, A> {

    private static final Comparator<GameActionEvent<?>> timeComparator = Comparator.comparingLong((GameActionEvent::getExecutionTimeMs));

    private final G game;
    private final Logger log;
    private final PriorityBlockingQueue<GameActionEvent<A>> actionEventQueue;
    private final BlockingQueue<GameUpdate<A>> gameUpdateQueue = new ArrayBlockingQueue<>(1024);
    //private final long[] array =  new long[600];
    int index = 0;

    private ScheduledExecutorService executorService;
    private Future<?> scheduledFuture;

    public ActionScheduler(G game, Logger log) {
        this.game = game;
        this.log = log;
        actionEventQueue = new PriorityBlockingQueue<>(1, timeComparator);
    }

    public ActionScheduler(G game, ActionScheduler<G, A> actionScheduler) {
        this.game = game;
        this.log = actionScheduler.log;
        var otherActionEventQueue = actionScheduler.getActionEventQueue();
        actionEventQueue = new PriorityBlockingQueue<>(otherActionEventQueue);
    }

    public void setExecutorService(ScheduledExecutorService executorService) {
        this.executorService = executorService;
    }

    public void scheduleActionEvent(GameActionEvent<A> actionEvent) {
        if (game.isRunning()) {
            var now = System.currentTimeMillis();
            // discard events that are scheduled before the current system time
            if (actionEvent.getExecutionTimeMs() < now) {
                addGameUpdate(new InvalidGameActionUpdate<>(actionEvent.getAction(), actionEvent.getPlayerId(), now, "Action got rejected because it was scheduled too early! The action was scheduled at " + actionEvent.getExecutionTimeMs() + "ms but the current timestamp is " + now + "ms."));
                log.debug("Discarding action with past execution time: " + (actionEvent.getExecutionTimeMs() - now));
                return;
            }
            // reschedule if the execution time of the new event is earlier than the current earliest
            if (actionEventQueue.isEmpty() || actionEvent.getExecutionTimeMs() < actionEventQueue.peek().getExecutionTimeMs()) {
                scheduleNext(actionEvent);
            }
            /*
            log.debug("exetime: " + (actionEvent.getExecutionTimeMs()));
            log.debug("now: " + now);
            log.debug("ahead: " + (actionEvent.getExecutionTimeMs() - now));

            array[index] = (actionEvent.getExecutionTimeMs() - now);
            index++;
            if(index == 20)
            {
                for (int i = 0; i < 500; i++)
                {
                    log.info("ahead " + array[i] + ", " + array[i + 1] + ", "+ array[i + 2] + ", "+ array[i + 3] + ", "+ array[i + 4] + ", "+ array[i+5] + ", "+ array[i+6] + ", "+ array[i+7] + ", "+ array[i + 8] + ", "+ array[i + 9] + ", " );
                    i += 10;
                }
            }

             */
        }
        actionEventQueue.add(actionEvent);
    }

    public BlockingQueue<GameActionEvent<A>> getActionEventQueue() {
        return actionEventQueue;
    }

    public GameUpdate<A> readGameUpdate() throws InterruptedException {
        return gameUpdateQueue.take();
    }

    /*
        Used when the game is running!
    */
    public void applyNextAction() {
        try {
            var actionEvent = actionEventQueue.take();
            var gameUpdates = game.applyActionEvent(actionEvent);
            for (var update : gameUpdates) {
                addGameUpdate(update);
            }
            if (!actionEventQueue.isEmpty()) {
                var nextEvent = actionEventQueue.peek();
                if (nextEvent.getExecutionTimeMs() < System.currentTimeMillis())
                    applyNextAction();
                else
                    scheduleNext(nextEvent);
            }
        } catch (InterruptedException e) {
            log.debug("Interrupted action scheduler");
            log.debugPrintStackTrace(e);
        }
    }

    /*
        Used for simulating!
        Expects an event queue with only internal actions and uses the applyAction
        method of the game directly.
    */
    public void applyUntil(long timeMs) throws ActionException {
        while (!actionEventQueue.isEmpty() && actionEventQueue.peek().getExecutionTimeMs() <= timeMs) {
            try {
                var actionEvent = actionEventQueue.take();
                game.applyAction(actionEvent.getAction(), actionEvent.getExecutionTimeMs());
            } catch (InterruptedException ignored) {
                //log.debug("interrupted");
            }
        }
    }

    public void clearUntil(long timeMs) {
        while (!actionEventQueue.isEmpty() && actionEventQueue.peek().getExecutionTimeMs() < timeMs) {
            try {
                actionEventQueue.take();
            } catch (InterruptedException ignored) {
            }
        }
    }

    public void start() {
        if (scheduledFuture == null && actionEventQueue.size() > 0)
            scheduleNext(actionEventQueue.peek());
    }

    public void stop() {
        if (scheduledFuture != null)
            scheduledFuture.cancel(true);

    }

    private void scheduleNext(GameActionEvent<A> actionEvent) {
        if (!game.isRunning() || executorService == null) return;
        if (scheduledFuture != null && !scheduledFuture.isCancelled() && !scheduledFuture.isDone())
            scheduledFuture.cancel(true);
        var now = System.currentTimeMillis();
        var timeTillNextActionEvent = actionEvent.getExecutionTimeMs() - now;
        scheduledFuture = executorService.schedule(this::applyNextAction, timeTillNextActionEvent, TimeUnit.MILLISECONDS);
    }

    private void addGameUpdate(GameUpdate<A> update) {
        gameUpdateQueue.add(update);
    }

    public void removeEqualActionAtSameTime(A action, long time) {
        List<GameActionEvent<A>> toRemove = new ArrayList<>();
        for (var actionEvent : actionEventQueue) {
            if (actionEvent.getExecutionTimeMs() == time) {
                if (action.equals(actionEvent.getAction())) {
                    toRemove.add(actionEvent);
                }
            }
        }
        if (toRemove.size() > 0) {
            for (var e : toRemove) {
                actionEventQueue.remove(e);
            }
        }
    }

}
