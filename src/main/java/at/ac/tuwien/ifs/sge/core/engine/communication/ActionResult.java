package at.ac.tuwien.ifs.sge.core.engine.communication;

public class ActionResult {
    private final boolean success;

    public ActionResult(boolean success) {
        this.success = success;
    }

    public boolean wasSuccessful() {
        return success;
    }
}
