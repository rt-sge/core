package at.ac.tuwien.ifs.sge.core.engine.communication.events;

import java.util.List;

public class InvalidGameActionEvent<A> extends GameUpdateEvent<A> {
    private final String errorMessage;


    public InvalidGameActionEvent(Integer playerId, List<A> actions, long executionTimeMs, String errorMessage) {
        super(playerId, actions, executionTimeMs);
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
