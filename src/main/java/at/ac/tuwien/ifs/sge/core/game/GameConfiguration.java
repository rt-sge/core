package at.ac.tuwien.ifs.sge.core.game;

import java.io.File;
import java.io.Serializable;

public interface GameConfiguration extends Serializable {

    static GameConfiguration fromString(String gameConfigString) {
        GameConfiguration config = new StringGameConfiguration(gameConfigString);
        if (gameConfigString != null) {
            var file = new File(gameConfigString);
            if (file.isFile())
                config = new FileGameConfiguration(file);
        }
        return config;
    }

    /**
     *
     * @param playerId id of player
     * @return the configuration for the specified player
     */
    GameConfiguration getConfigurationForPlayer(int playerId);

}
