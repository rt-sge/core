package at.ac.tuwien.ifs.sge.core.util.node;

import at.ac.tuwien.ifs.sge.core.game.RealTimeGame;

import java.util.Collections;
import java.util.Stack;

public class RealTimeGameNode<A> {

    // the player id of the player who determines the NEXT action
    protected final int playerId;

    protected final RealTimeGame<A, ?> game;
    protected final A responsibleAction;

    private final Stack<A> unexploredActions = new Stack<>();

    public RealTimeGameNode(int playerId, RealTimeGame<A, ?> game, A responsibleAction) {
        this.game = game;
        this.playerId = playerId;
        this.responsibleAction = responsibleAction;
        unexploredActions.addAll(game.getPossibleActions(playerId));
        Collections.shuffle(unexploredActions);
    }

    public int getNrOfUnexploredActions() {
        return unexploredActions.size();
    }

    public boolean hasUnexploredActions() {
        return !unexploredActions.isEmpty();
    }

    public A popUnexploredAction() {
        if (!unexploredActions.isEmpty())
            return unexploredActions.pop();
        return null;
    }

    public int getNextPlayerId() {
        return (playerId + 1) % game.getNumberOfPlayers();
    }

    public int getPlayerId() {
        return playerId;
    }

    public A getResponsibleAction() {
        return responsibleAction;
    }

    public RealTimeGame<A, ?> getGame() {
        return game;
    }

}
