package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.game.TurnBasedGame;

public class TurnBasedHumanAgent<G extends TurnBasedGame<A, ?>, A> extends AbstractTurnBasedHumanAgent<G, A> {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        var playerId = getPlayerIdFromArgs(args);
        var playerName = getPlayerNameFromArgs(args);
        var gameClass = ( Class<? extends TurnBasedGame<Object, Object>> ) getGameClassFromArgs(args);
        var agent = new TurnBasedHumanAgent<>(gameClass, playerId, playerName);
        agent.start();
    }

    public TurnBasedHumanAgent(Class<G> clazz, int playerId, String playerName) {
        super(clazz, playerId, playerName);
    }
}
