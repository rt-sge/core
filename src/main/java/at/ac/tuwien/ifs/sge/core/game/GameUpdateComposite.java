package at.ac.tuwien.ifs.sge.core.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class GameUpdateComposite<A> {

    private final long executionTimeMs;

    private final HashMap<Integer, List<A>> actionsByPlayers = new HashMap<>();

    public GameUpdateComposite(long executionTimeMs) {
        this.executionTimeMs = executionTimeMs;
    }

    public void addActionForPlayers(A action, Collection<Integer> players) {
        players.forEach(player -> addActionForPlayer(action, player));
    }

    public void addActionForPlayer(A action, Integer player) {
        var actions = actionsByPlayers.getOrDefault(player, new ArrayList<>());
        actions.add(action);
        actionsByPlayers.putIfAbsent(player, actions);
    }
    public List<GameUpdate<A>> toList() {
        return actionsByPlayers.entrySet().stream().map(kv -> new GameUpdate<>(kv.getValue(), kv.getKey(), executionTimeMs)).toList();
    }
}
