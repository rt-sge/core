package at.ac.tuwien.ifs.sge.core.game;

import java.util.Set;

/**
 * A game.
 *
 * @param <A> - Type of Action
 * @param <B> - Type of Board
 */
public interface Game<A, B> {

  //region Configuration

  /**
   * Returns the game configuration.
   *
   * @return the configuration
   */
  GameConfiguration getGameConfiguration();

  /**
   * Checks if the game supplies an implementation of {@link GameConfiguration} that diverges for
   * different players. E.g. games with partial information. Defaults to false.
   *
   * @return the configuration
   */
  default boolean hasDivergingGameConfiguration() { return false; }

  /**
   * Gets the class name of the game's configuration editor. The supplied class has to implement the
   * JavaFXConfigEditor interface found in the GUI library.
   * @return the class name of the configuration editor class
   */
  default String getConfigurationEditorClassName() { return null; }

  /**
   * Check how many players have to play.
   *
   * @return the minimum number of players
   */
  int getMinimumNumberOfPlayers();

  /**
   * Check how many players can play at most.
   *
   * @return the maximum number of players.
   */
  int getMaximumNumberOfPlayers();

  /**
   * Checks if the game is canonical. A game is canonical if it bears all information and is not the
   * child of a non-canonical game.
   *
   * @return true if canonical
   */
  boolean isCanonical();

  /**
   * Checks if the game is played in realtime.
   *
   * @return true if played in realtime
   */
  boolean isRealtime();

  /**
   * Gets the class name of the human agent for this game
   * When it returns null the default implementation of a human agent is used
   *
   * @return human agent class name
   */
  default String getHumanAgentClassName() { return null; }


  /**
   * Gets the visualization type of the game. Default is visualization by text.
   * The commandline application supports:
   *    - {@link VisualizationType#text}
   *
   * The graphical user interface supports:
   *    - {@link VisualizationType#text}
   *    - {@link VisualizationType#javafx}
   *
   * @return the visualization type of the game
   */
  default VisualizationType getVisualizationType() { return VisualizationType.text; }


  /**
   * Gets the class name of the class responsible for visualization of the game.
   * Depending on the {@link VisualizationType} the class has to implement the interface
   * JavaFXVisualization found in the GUI library.
   * Defaults to null which is appropriate for {@link VisualizationType#text}
   *
   * @return the visualization type of the game
   */
  default String getVisualizationClassName() { return null; }

  //endregion

  //region General

  /**
   * Checks whether the game is over yet..
   *
   * @return true if and only if game is over
   */
  boolean isGameOver();

  /**
   * Counts the number of involved players. Has to be between getMinimumNumberOfPlayers() and
   * getMaximumNumberOfPlayers(). This number cannot change throughout the game.
   *
   * @return the number of players
   */
  int getNumberOfPlayers();

  /**
   * Returns the board. Notice that in non-canonical games some information
   * might be hidden.
   *
   * @return the board
   */
  B getBoard();

  /**
   * A representation of the current game via text. Can be multiline. Per default toString.
   *
   * @return a string representing the game
   */
  default String toTextRepresentation() {
    return this.toString();
  }


  /**
   * Disqualifies the player from the game before it is over
   *
   * @param playerId of the player to
   * @return true if the player was disqualified successfully
   */
  default boolean disqualifyPlayer(int playerId) { return false; }

  //endregion

  //region Utility

  /**
   * Applies the (public) utility function for the given player. The utility function is the final
   * measure which determines how "good" a player does. The player with the highest value is
   * considered the winner. On equality it is considered a tie.
   *
   * @param player - the player
   * @return the result of the utility function for the player
   */
  double getUtilityValue(int player);

  /**
   * Applies the (public) utility function of each player and returns the result in an array where
   * the index corresponds to the player id.
   *
   * @return the result of the utility function for each player
   */
  default double[] getGameUtilityValue() {
    double[] utilityValues = new double[getNumberOfPlayers()];
    for (int p = 0; p < utilityValues.length; p++) {
      utilityValues[p] = getUtilityValue(p);
    }
    return utilityValues;
  }

  /**
   * The weight of the utility function of a given player. Per default 0 if player or the
   * currentPlayer is less than 0 and 1 iff getCurrentPlayer() equals player otherwise -1.
   *
   * @param player - the player
   * @return the weight of the utility function.
   */
  double getPlayerUtilityWeight(int player);

  /**
   * Applies the (public) utility function of each player and returns the result multiplied with
   * their weight in a sum.
   * <p>
   * Should no weight be supplied for a given player's utility function getPlayerUtilityWeight() is
   * used instead. Any number of weights can be given.
   *
   * @param weights - the optional weights
   * @return the weighted sum of the utility functions
   */
  default double getUtilityValue(double... weights) {
    double value = 0;
    for (int p = 0; p < getNumberOfPlayers(); p++) {
      if (p < weights.length) {
        value += weights[p] * getUtilityValue(p);
      } else {
        value += getPlayerUtilityWeight(p) * getUtilityValue(p);
      }
    }
    return value;
  }

  //endregion

  //region Heuristic

  /**
   * Applies the heuristic function of each player and returns the result in an array where the
   * index corresponds to the player id.
   *
   * @return the result of the utility function for each player
   */
  default double[] getGameHeuristicValue() {
    double[] heuristicValues = new double[getNumberOfPlayers()];

    for (int p = 0; p < heuristicValues.length; p++) {
      heuristicValues[p] = getHeuristicValue(p);
    }

    return heuristicValues;
  }


  /**
   * The weight of the heuristic function of a given player. Per default the same as
   * getPlayerUtilityWeight().
   *
   * @param player - the player
   * @return the weight of the heuristic function.
   */
  default double getPlayerHeuristicWeight(int player) {
    return getPlayerUtilityWeight(player);
  }


  /**
   * Applies the heuristic function of each player and returns the result multiplied with their
   * weight in a sum.
   * <p>
   * Should no weight be supplied for a given player's utility function getPlayerHeuristicWeight()
   * is used instead. Any number of weights can be given.
   *
   * @param weights - the optional weights
   * @return the weighted sum of the heuristic functions
   */
  default double getHeuristicValue(double... weights) {
    double value = 0;
    for (int p = 0; p < getNumberOfPlayers(); p++) {
      if (p < weights.length) {
        value += weights[p] * getHeuristicValue(p);
      } else {
        value += getPlayerHeuristicWeight(p) * getHeuristicValue(p);
      }
    }
    return value;
  }

  /**
   * Applies the heuristic function for the given player. This function is a more lax measure in how
   * "good" a player does, it is not used to determine the outcome of a game. Per default the same
   * as getUtilityValue().
   *
   * @param player - the player
   * @return the result of the heuristic function for the player
   */
  default double getHeuristicValue(int player) {
    return getUtilityValue(player);
  }

  //endregion

  //region Actions

  /**
   * Collects all possible moves and returns them as a set. Should the game be over an empty set is
   * returned instead.
   *
   * @return a set of all possible moves
   */
  Set<A> getPossibleActions();


  /**
   * Checks whether an action is valid.
   *
   * @param action - the action
   * @return true - if the action is valid and possible
   */
  default boolean isValidAction(A action) {
    return getPossibleActions().contains(action);
  }


  /**
   * Returns how many actions were taken.
   *
   * @return how many actions were taken
   */
  int getNumberOfActions();

  /**
   * Returns the action records concatenated as one string.
   *
   * @return concatenated action record string
   */
  String getActionRecordString();

  //endregion

}
