package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.game.TurnBasedGame;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

abstract public class AbstractTurnBasedHumanAgent<G extends TurnBasedGame<A, ?>, A> extends AbstractTurnBasedGameAgent<G, A> {

    private final Scanner scanner;

    public AbstractTurnBasedHumanAgent(Class<G> clazz, int playerId, String playerName) {
        super(clazz, playerId, playerName, 0);
        scanner = new Scanner(System.in);
    }

    @Override
    public A computeNextAction(G game, long computationTime, TimeUnit timeUnit) {
        this.setTimers(computationTime, timeUnit);
        var possibleActions = new ArrayList<>(game.getPossibleActions());
        A action = null;
        do {
            log.info("Possible actions: ");
            for (var i = 0; i < possibleActions.size(); i++) {
                log.info("[" + i + "] " + possibleActions.get(i).toString());
            }
            log.info("Enter the nr of the action to perform: ");
            var line = scanner.nextLine();
            log.info("Your input: " + line);
            try {
                var actionNr = Integer.parseInt(line);
                if (actionNr >= 0 && actionNr < possibleActions.size()) {
                    action = possibleActions.get(actionNr);
                    log.info("Sending action: " + action);
                }
            } catch (NumberFormatException ignored) {
                log.info("Invalid input!");
                log._info_();
            }
        } while (action == null && !shouldStopComputation());
        return action;
    }

}
