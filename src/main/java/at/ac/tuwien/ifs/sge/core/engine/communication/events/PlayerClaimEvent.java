package at.ac.tuwien.ifs.sge.core.engine.communication.events;

public class PlayerClaimEvent extends SgeEvent {

    private final String playerName;

    public PlayerClaimEvent(String playerName) {
        this.playerName = playerName;
    }

    public PlayerClaimEvent(Integer playerId, String playerName) {
        super(playerId);
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }


    @Override
    public String toString() {
        return playerName + "#" + playerId;
    }
}
