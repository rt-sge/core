package at.ac.tuwien.ifs.sge.core.security;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.crypto.util.OpenSSHPrivateKeyUtil;
import org.bouncycastle.crypto.util.OpenSSHPublicKeyUtil;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.jcajce.spec.OpenSSHPrivateKeySpec;
import org.bouncycastle.jcajce.spec.OpenSSHPublicKeySpec;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Date;

public class SecurityUtil {
    public static final String BEGIN_CERT = "-----BEGIN CERTIFICATE-----";
    public static final String END_CERT = "-----END CERTIFICATE-----";

    public static String x509CertToPem(X509Certificate certificate) throws CertificateEncodingException {
        var certPem = BEGIN_CERT + "\n";
        certPem += Base64.getEncoder().encodeToString(certificate.getEncoded()) + "\n";
        certPem += END_CERT + "\n";
        return certPem;
    }

    public static String keyToBase64String(Key key) {
        byte[] bytes = key.getEncoded();
        return Base64.getEncoder().encodeToString(bytes);
    }

    public static PrivateKey base64StringToEd25519PrivateKey(String base64) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, IOException {
        var bytes = Base64.getDecoder().decode(base64);
        var keyFactory = KeyFactory.getInstance("Ed25519", "BC");
        var keySpec = new OpenSSHPrivateKeySpec(OpenSSHPrivateKeyUtil.encodePrivateKey(PrivateKeyFactory.createKey(bytes)));
        return keyFactory.generatePrivate(keySpec);
    }

    public static PublicKey base64StringToEd25519PublicKey(String base64) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, IOException {
        var bytes = Base64.getDecoder().decode(base64);
        var keyFactory = KeyFactory.getInstance("Ed25519", "BC");
        var keySpec = new OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(PublicKeyFactory.createKey(bytes)));
        return keyFactory.generatePublic(keySpec);
    }

    public static  KeyPair generateEd25519KeyPair(SecureRandom secureRandom) throws NoSuchAlgorithmException {
        var keyPairGenerator = KeyPairGenerator.getInstance("Ed25519");
        keyPairGenerator.initialize(256, secureRandom);
        return keyPairGenerator.generateKeyPair();
    }

    public static X509Certificate generateSelfSignedEd25519Certificate(String dn, KeyPair keyPair) throws GeneralSecurityException, IOException, OperatorCreationException {
        return generateSignedEd25519Certificate(dn, keyPair.getPublic(), keyPair.getPrivate());
    }

    public static X509Certificate generateSignedEd25519Certificate(String dn, PublicKey publicKey, PrivateKey signerKey) throws GeneralSecurityException, IOException, OperatorCreationException {
        var from = new Date();
        var to = new Date(from.getTime() + 365 * 1000L * 24L * 60L * 60L);
        var serialNumber = new BigInteger(64, new SecureRandom());
        var owner = new X500Name(dn);

        var certificateBuilder = new JcaX509v3CertificateBuilder(owner, serialNumber, from, to, owner, publicKey);
        var signer = new JcaContentSignerBuilder("Ed25519").build(signerKey);

        var certificateHolder = certificateBuilder.build(signer);
        return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(certificateHolder.getEncoded()));

    }
}
