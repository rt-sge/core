package at.ac.tuwien.ifs.sge.core.engine.communication.events;

import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;

public class SetUpEvent extends SgeEvent {

    private GameConfiguration gameConfiguration;

    private int numberOfPlayers;


    public SetUpEvent() {
    }

    public SetUpEvent(int numberOfPlayers)  {
        this.numberOfPlayers = numberOfPlayers;
    }

    public SetUpEvent(GameConfiguration gameConfiguration, int numberOfPlayers)  {
        this.numberOfPlayers = numberOfPlayers;
        this.gameConfiguration = gameConfiguration;
    }


    public void setGameConfiguration(GameConfiguration gameConfiguration) {
        this.gameConfiguration = gameConfiguration;
    }

    public GameConfiguration getGameConfiguration() {
        return gameConfiguration;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }
}
