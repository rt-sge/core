package at.ac.tuwien.ifs.sge.core.agent;

public class MemorySpecifications {

    private final String minHeapSize;
    private final String maxHeapSize;
    private final String threadStackSize;

    public MemorySpecifications(String minHeapSize, String maxHeapSize, String threadStackSize) {
        var minHeapSizeBytes = validateSize(minHeapSize, 1024 * 1024, "Min Heap Size", "1M");
        var maxHeapSizeBytes = validateSize(maxHeapSize, 2 * 1024 * 1024, "Max Heap Size", "2M");
        var threadStackSizeBytes = validateSize(threadStackSize, 64 * 1024, "Thread Stack Size", "64k");
        if (minHeapSizeBytes > maxHeapSizeBytes)
            throw new IllegalArgumentException("Min Heap Size can not be larger than Max Heap Size.");
        this.minHeapSize = minHeapSize;
        this.maxHeapSize = maxHeapSize;
        this.threadStackSize = threadStackSize;
    }

    public String getMinHeapSize() {
        return minHeapSize;
    }

    public String getMaxHeapSize() {
        return maxHeapSize;
    }

    public String getThreadStackSize() {
        return threadStackSize;
    }

    private long validateSize(String sizeString, int lowerBoundary, String name, String lowerBoundaryName) {
        var value = getValueFromString(sizeString);
        if (value < 0)
            throw new IllegalArgumentException(name + " is not a number.");
        var lastChar = sizeString.charAt(sizeString.length() - 1);
        var tooSmallException = new IllegalArgumentException(name + " smaller than " + lowerBoundaryName + ".");
        long byteValue = value;
        switch (lastChar) {
            case 'k', 'K' -> {
                byteValue = 1024L * value;
                if ((byteValue / lowerBoundary) < 1)
                    throw tooSmallException;
            }
            case 'm', 'M' -> {
                byteValue = 1024L * 1024 * value;
                if ((byteValue / lowerBoundary) < 1)
                    throw tooSmallException;
            }
            case 'g', 'G' -> {
                byteValue = 1024L * 1024 * 1024 * value;
                if ((byteValue / lowerBoundary) < 1)
                    throw tooSmallException;
            }
            default -> {
                if ((value % 1024) != 0)
                    throw new IllegalArgumentException(name + " is not a multiple of 1024.");
                if ((value / lowerBoundary) < 1)
                    throw tooSmallException;
            }
        }
        return byteValue;
    }

    private int getValueFromString(String sizeString) {
        var stringValue = sizeString.substring(0, sizeString.length() - 1);
        try {
            return Integer.parseInt(stringValue);
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}
