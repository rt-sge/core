package at.ac.tuwien.ifs.sge.core.engine.game;

import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.core.agent.Agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class MatchResult {


  public static StringBuilder appendSeparationLine(StringBuilder stringBuilder,
                                                   int firstColumnWidth,
                                                   List<Integer> widths) {
    stringBuilder.append('+');
    Util.appendNTimes(stringBuilder, '-', firstColumnWidth + 2).append('+');

    for (Integer width : widths) {
      Util.appendNTimes(stringBuilder, '-', width + 2).append('+');
    }

    stringBuilder.append('\n');
    return stringBuilder;
  }

  private final HashMap<Integer, Agent> gameAgents;
  private final long duration;
  private final double[] result;
  private String string = null;

  public MatchResult(HashMap<Integer, Agent> gameAgents, long startTime, long endTime, double[] result) {
    this(gameAgents, endTime - startTime, result);
  }

  public MatchResult(HashMap<Integer, Agent> gameAgents, long duration, double[] result) {
    this.gameAgents = gameAgents;
    this.duration = duration;
    this.result = result;
  }

  public List<Agent> getGameAgents() {
    return new ArrayList<>(gameAgents.values());
  }

  public HashMap<Integer, Agent> getGameAgentsByPlayerId() {
    return gameAgents;
  }

  public long getDuration() {
    return duration;
  }

  public double[] getResult() {
    return result;
  }

  public HashMap<Agent, Double> getResultByAgent() {
    var map = new HashMap<Agent, Double>();
    for (var i = 0; i < result.length; i++) {
      map.put(gameAgents.get(i), result[i]);
    }
    return map;
  }


  @Override
  public String toString() {
    if (string != null) {
      return string;
    }
    StringBuilder stringBuilder = new StringBuilder();

    List<String> gameAgentNames = gameAgents.entrySet().stream().map( kv -> kv.getValue().toString() + " (" + kv.getKey() + ")")
            .collect(Collectors.toList());
    List<Integer> gameAgentWidths = gameAgentNames.stream().map(String::length)
        .collect(Collectors.toList());

    double[] score = Util.scoresOutOfUtilities(result);
    String scoreString = "Score";
    String utilityString = "Utility";
    int firstColumnWidth = Math.max(scoreString.length(), utilityString.length());

    appendSeparationLine(stringBuilder, firstColumnWidth, gameAgentWidths).append('|');
    Util.appendNTimes(stringBuilder, ' ', firstColumnWidth + 2);
    stringBuilder.append('|').append(' ').append(String.join(" | ", gameAgentNames)).append(' ')
        .append('|')
        .append('\n');
    appendSeparationLine(stringBuilder, firstColumnWidth, gameAgentWidths).append('|');

    Util.appendWithBlankBuffer(stringBuilder, scoreString, firstColumnWidth + 2).append('|');

    for (int i = 0; i < gameAgentWidths.size(); i++) {
      int width = gameAgentWidths.get(i);
      stringBuilder.append(' ');
      Util.appendWithBlankBuffer(stringBuilder, score[i], width).append(' ').append('|');
    }

    stringBuilder.append('\n').append('|');

    Util.appendWithBlankBuffer(stringBuilder, utilityString, firstColumnWidth + 2).append('|');

    for (int i = 0; i < gameAgentWidths.size(); i++) {
      int width = gameAgentWidths.get(i);
      stringBuilder.append(' ');
      Util.appendWithBlankBuffer(stringBuilder, result[i], width).append(' ').append('|');
    }

    stringBuilder.append('\n');
    appendSeparationLine(stringBuilder, firstColumnWidth, gameAgentWidths)
        .deleteCharAt(stringBuilder.length() - 1);
    string = stringBuilder.toString();
    return string;
  }

}
