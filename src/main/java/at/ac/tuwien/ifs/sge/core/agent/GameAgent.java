package at.ac.tuwien.ifs.sge.core.agent;

import at.ac.tuwien.ifs.sge.core.game.Game;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;

public interface GameAgent<G extends Game<A, ?>, A> {

  /**
   * Called before the match to allow the agent to be set up.
   *
   * @param gameConfiguration used configuration for this game
   * @param numberOfPlayers number of players participating in this match
   */
  void setup(GameConfiguration gameConfiguration, int numberOfPlayers);


  /**
   * Called after the match is done.
   */
  void shutdown();

}
