module sge.core {
    requires io.reactivex.rxjava3;
    requires com.google.common;
    requires org.bouncycastle.provider;
    requires org.bouncycastle.pkix;

    exports at.ac.tuwien.ifs.sge.core.agent;

    exports at.ac.tuwien.ifs.sge.core.util;
    exports at.ac.tuwien.ifs.sge.core.util.node;
    exports at.ac.tuwien.ifs.sge.core.util.pair;
    exports at.ac.tuwien.ifs.sge.core.util.tree;
    exports at.ac.tuwien.ifs.sge.core.util.unit;

    exports at.ac.tuwien.ifs.sge.core.game;
    exports at.ac.tuwien.ifs.sge.core.game.exception;

    exports at.ac.tuwien.ifs.sge.core.engine.communication;
    exports at.ac.tuwien.ifs.sge.core.engine.communication.events;

    exports at.ac.tuwien.ifs.sge.core.engine.factory;
    exports at.ac.tuwien.ifs.sge.core.engine.loader;
    exports at.ac.tuwien.ifs.sge.core.engine.logging;

    exports at.ac.tuwien.ifs.sge.core.engine.game;
    exports at.ac.tuwien.ifs.sge.core.engine.game.match;
    exports at.ac.tuwien.ifs.sge.core.engine.game.tournament;
    exports at.ac.tuwien.ifs.sge.core.security;


}
