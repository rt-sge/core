# Real Time Strategy Game Engine

The core library for the Real Time Strategy Game Engine developed for the [IFS](https://informatics.tuwien.ac.at/) at the [Technical University Vienna](https://www.tuwien.at/)
This library is neccessary for any game and agent development.

# Installation
Available through jitpack
[![Release](https://jitpack.io/v/com.gitlab.rt-sge/core.svg)](https://jitpack.io/#com.gitlab.rt-sge/core/)

**Gradle:** 

Add the JitPack repository to your build.gradle
```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```
Add the RT-SGE dependency
```
dependencies {
    implementation 'com.gitlab.rt-sge:core:{version}'
}
```
**Other build automation tools:** check [JitPack](https://jitpack.io/#com.gitlab.rt-sge/core/).

# Documentation
Refer to the [GitLab Wiki](https://gitlab.com/rt-sge/core/-/wikis/home) for documentation on how to develop games and agents.
